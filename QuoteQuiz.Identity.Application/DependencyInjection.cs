﻿using System.Linq;
using System.Reflection;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using QuoteQuiz.Common.Application.Common.Behaviors;

namespace QuoteQuiz.Identity.Application
{
    public static class DependencyInjection
    {
        public static void AddIValidatorAssembly(this IServiceCollection services)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.GetInterfaces()
                .Any(gi => gi.IsGenericType && gi.GetGenericTypeDefinition() == typeof(IValidator<>))).FirstOrDefault();

            services.AddValidatorsFromAssemblyContaining(typesToRegister);
        }

        public static IServiceCollection AddIdentityApplication(this IServiceCollection services)
        {
            services.AddIValidatorAssembly();
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorPipelineBehavior<,>));

            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}
