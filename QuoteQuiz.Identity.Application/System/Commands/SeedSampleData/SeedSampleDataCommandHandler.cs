﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.ValueObjects;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Identity.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommandHandler : ICommandHandler<SeedSampleDataCommand>
    {
        private readonly IUserManagerService _userManagerService;
        private readonly IUserRoleManagerService _userRoleManagerService;

        public SeedSampleDataCommandHandler(IUserManagerService userManagerService, IUserRoleManagerService userRoleManagerService)
        {
            _userManagerService = userManagerService;
            _userRoleManagerService = userRoleManagerService;
        }
        public async Task<Unit> Handle(SeedSampleDataCommand request, CancellationToken cancellationToken)
        {
            var names = Enum.GetNames(typeof(ApplicationUserType));

            foreach (var roleName in names)
            {
                var roleExist = await _userRoleManagerService.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    await _userRoleManagerService.CreateUserRoleAsync(roleName);
                }
            }

            var user = new ApplicationUser(
                Guid.NewGuid(),
                ApplicationUserType.User,
                "demouser@demo.com",
                "Demo",
                "Spa",
                PhoneNumber.For("577123456")
            );

            var userExist = await _userManagerService.UserExistsAsync(user.Email);
            if (!userExist)
            {
                var result = await _userManagerService.CreateUserAsync(user, ApplicationUserType.User, "Pass@word1");
            }

            var admin = new ApplicationUser(
                Guid.NewGuid(),
                ApplicationUserType.Admin,
                "demoadmin@demo.com",
                "Demo",
                "Spa",
                PhoneNumber.For("577123456")
            );

            var adminExist = await _userManagerService.UserExistsAsync(admin.Email);
            if (!adminExist)
            {
                var result = await _userManagerService.CreateUserAsync(admin, ApplicationUserType.Admin, "Pass@word1");
            }

            return Unit.Value;
        }
    }
}
