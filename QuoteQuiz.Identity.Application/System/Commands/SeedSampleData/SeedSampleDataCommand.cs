﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;

namespace QuoteQuiz.Identity.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommand : ICommand { }
}
