﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.GetApplicationUser
{
    public class GetApplicationUserQuery : IQuery<GetApplicationUserVM>
    {
        public Guid ApplicationUserId { get; set; }
    }
}
