﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.GetApplicationUser
{
    public class GetApplicationUserQueryValidator : AbstractValidator<GetApplicationUserQuery>
    {
        public GetApplicationUserQueryValidator()
        {
            RuleFor(x => x.ApplicationUserId).NotNull();
        }
    }
}
