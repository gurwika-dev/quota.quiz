﻿namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.GetApplicationUser
{
    public class GetApplicationUserVM
    {
        public GetApplicationUserDTO Entry { get; set; }
    }
}
