﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.Exceptions;
using System;
using QuoteQuiz.Common.Application.Common.Handlers;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.GetApplicationUser
{
    public class GetApplicationUserQueryHandler : QueryHandler, IQueryHandler<GetApplicationUserQuery, GetApplicationUserVM>
    {
        public GetApplicationUserQueryHandler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<GetApplicationUserVM> Handle(GetApplicationUserQuery request, CancellationToken cancellationToken)
        {
            var entry = _context.Set<ApplicationUser>()
                                        .Where(x => x.Type == ApplicationUserType.User && x.Id == request.ApplicationUserId.ToString())
                                        .ProjectTo<GetApplicationUserDTO>(_mapper.ConfigurationProvider)
                                        .FirstOrDefault();

            if (entry == null)
            {
                throw new DomainException("User does not exists");
            }

            return await Task.Run(() =>
            {
                return new GetApplicationUserVM
                {
                    Entry = entry
                };
            });
        }
    }
}
