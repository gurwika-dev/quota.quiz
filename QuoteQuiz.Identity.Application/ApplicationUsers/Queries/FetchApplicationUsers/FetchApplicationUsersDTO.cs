﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Application;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{
    public class FetchApplicationUsersDTO : IMapFrom<ApplicationUser>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public bool Disabled { get; protected set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ApplicationUser, FetchApplicationUsersDTO>()
                 .ForMember(
                    dest => dest.Id,
                    opt => opt.MapFrom(src => Guid.Parse(src.Id))
                );
        }
    }
}
