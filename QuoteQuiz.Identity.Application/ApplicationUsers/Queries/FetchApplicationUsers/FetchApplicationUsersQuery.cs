﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{
    public class FetchApplicationUsersQuery : IQuery<FetchApplicationUsersVM>
    {
        public FetchApplicationUsersModel Model { get; set; }
    }
}
