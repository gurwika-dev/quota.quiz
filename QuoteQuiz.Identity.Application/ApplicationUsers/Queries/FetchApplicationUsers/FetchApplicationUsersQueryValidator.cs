﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{
    public class FetchApplicationUsersQueryValidator : AbstractValidator<FetchApplicationUsersQuery>
    {
        public FetchApplicationUsersQueryValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Page).GreaterThanOrEqualTo(1).NotNull();
            RuleFor(x => x.Model.Limit).GreaterThan(1).NotNull();
            RuleFor(x => x.Model.KeyWord).MaximumLength(50);
        }
    }
}
