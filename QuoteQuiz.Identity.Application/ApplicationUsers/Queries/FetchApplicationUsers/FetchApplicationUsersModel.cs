﻿namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{
    public class FetchApplicationUsersModel
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string KeyWord { get; set; }
    }
}
