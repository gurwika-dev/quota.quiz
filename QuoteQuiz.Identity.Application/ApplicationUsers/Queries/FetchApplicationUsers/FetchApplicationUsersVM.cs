﻿using System.Collections.Generic;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{
    public class FetchApplicationUsersVM
    {
        public IList<FetchApplicationUsersDTO> Entries { get; set; }
        public int TotalCount { get; set; }
    }
}
