﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Application.Common.Handlers;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers
{

    public class FetchApplicationUsersQueryHandler : QueryHandler, IQueryHandler<FetchApplicationUsersQuery, FetchApplicationUsersVM>
    {
        public FetchApplicationUsersQueryHandler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<FetchApplicationUsersVM> Handle(FetchApplicationUsersQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<ApplicationUser>().Where(x => x.Type == ApplicationUserType.User);


            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                entriesQuery = entriesQuery.Where(e => e.FirstName.Contains(request.Model.KeyWord) ||
                                                e.LastName.Contains(request.Model.KeyWord) ||
                                            e.PhoneNumber.Contains(request.Model.KeyWord)
                                        );
            }

            var totalCount = entriesQuery.Count();
            var entries = await entriesQuery
                                    .ProjectTo<FetchApplicationUsersDTO>(_mapper.ConfigurationProvider)
                                    .Skip(request.Model.Limit * (request.Model.Page - 1))
                                    .Take(request.Model.Limit)
                                    .ToListAsync(cancellationToken);

            return new FetchApplicationUsersVM
            {
                Entries = entries,
                TotalCount = totalCount
            };
        }
    }
}
