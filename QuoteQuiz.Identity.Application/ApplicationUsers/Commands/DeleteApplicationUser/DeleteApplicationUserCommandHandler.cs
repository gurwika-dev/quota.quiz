﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DeleteApplicationUser
{
    public class DeleteApplicationUserCommandHandler : CommandHandler, ICommandHandler<DeleteApplicationUserCommand>
    {
        private readonly IUserManagerService _userManagerService;

        public DeleteApplicationUserCommandHandler(IApplicationDbContext context, IEventBus eventBus, IUserManagerService userManagerService) : base(context, eventBus)
        {
            _userManagerService = userManagerService;
        }

        public async Task<Unit> Handle(DeleteApplicationUserCommand request, CancellationToken cancellationToken)
        {
            var user = _context.Set<ApplicationUser>()
                                        .Where(x => x.Type == ApplicationUserType.User && x.Id == request.ApplicationUserId.ToString())
                                        .FirstOrDefault();

            if (user == null)
            {
                throw new DomainException("User does not exists");
            }

            await _userManagerService.DeleteUserAsync(user.Id);

            return Unit.Value;
        }
    }
}
