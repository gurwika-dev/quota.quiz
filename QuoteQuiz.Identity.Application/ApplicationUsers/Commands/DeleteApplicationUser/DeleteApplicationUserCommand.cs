﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DeleteApplicationUser
{
    public class DeleteApplicationUserCommand : ICommand
    {
        public Guid ApplicationUserId { get; set; }
    }
}
