﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DeleteApplicationUser
{
    public class DeleteApplicationUserCommandValidator : AbstractValidator<DeleteApplicationUserCommand>
    {
        public DeleteApplicationUserCommandValidator()
        {
            RuleFor(x => x.ApplicationUserId).NotNull();
        }
    }
}
