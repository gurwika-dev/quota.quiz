﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DisableApplicationUser
{
    public class DisableApplicationUserCommandValidator : AbstractValidator<DisableApplicationUserCommand>
    {
        public DisableApplicationUserCommandValidator()
        {
            RuleFor(x => x.ApplicationUserId).NotNull();
        }
    }
}
