﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DisableApplicationUser
{
    public class DisableApplicationUserCommand : ICommand
    {
        public Guid ApplicationUserId { get; set; }
    }
}
