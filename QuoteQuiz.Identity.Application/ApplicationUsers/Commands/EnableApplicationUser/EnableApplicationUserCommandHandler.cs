﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.EnableApplicationUser
{
    public class EnableApplicationUserCommandHandler : CommandHandler, ICommandHandler<EnableApplicationUserCommand>
    {
        public EnableApplicationUserCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus)
        {
        }

        public async Task<Unit> Handle(EnableApplicationUserCommand request, CancellationToken cancellationToken)
        {
            var user = _context.Set<ApplicationUser>()
                                        .Where(x => x.Type == ApplicationUserType.User && x.Id == request.ApplicationUserId.ToString())
                                        .FirstOrDefault();

            if (user == null)
            {
                throw new DomainException("User does not exists");
            }

            user.Enable();

            await SaveAndPublish(user, cancellationToken);

            return Unit.Value;
        }
    }
}
