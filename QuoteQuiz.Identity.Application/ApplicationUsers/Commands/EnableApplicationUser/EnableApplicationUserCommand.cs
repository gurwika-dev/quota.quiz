﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.EnableApplicationUser
{
    public class EnableApplicationUserCommand : ICommand
    {
        public Guid ApplicationUserId { get; set; }
    }
}
