﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.EnableApplicationUser
{
    public class EnableApplicationUserCommandValidator : AbstractValidator<EnableApplicationUserCommand>
    {
        public EnableApplicationUserCommandValidator()
        {
            RuleFor(x => x.ApplicationUserId).NotNull();
        }
    }
}
