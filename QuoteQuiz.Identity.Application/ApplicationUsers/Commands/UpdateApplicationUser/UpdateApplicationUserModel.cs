﻿using QuoteQuiz.Common.Domain.ValueObjects;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.UpdateApplicationUser
{
    public class UpdateApplicationUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
    }
}
