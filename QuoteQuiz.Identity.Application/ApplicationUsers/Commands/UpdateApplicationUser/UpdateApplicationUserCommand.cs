﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.UpdateApplicationUser
{
    public class UpdateApplicationUserCommand : ICommand
    {
        public Guid ApplicationUserId { get; set; }
        public UpdateApplicationUserModel Model { get; set; }
    }
}
