﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.UpdateApplicationUser
{
    public class UpdateApplicationUserCommandValidator : AbstractValidator<UpdateApplicationUserCommand>
    {
        public UpdateApplicationUserCommandValidator()
        {
            RuleFor(x => x.ApplicationUserId).NotNull();
            RuleFor(x => x.Model.FirstName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();
            RuleFor(x => x.Model.LastName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();

            RuleFor(x => x.Model.PhoneNumber).NotNull();
        }
    }
}
