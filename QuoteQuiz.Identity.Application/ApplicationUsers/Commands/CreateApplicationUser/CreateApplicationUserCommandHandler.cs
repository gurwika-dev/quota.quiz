﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.Exceptions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.CreateApplicationUser
{
    public class CreateApplicationUserCommandHandler : CommandHandler, ICommandHandler<CreateApplicationUserCommand>
    {
        private readonly IUserManagerService _userManagerService;

        public CreateApplicationUserCommandHandler(IApplicationDbContext context, IEventBus eventBus, IUserManagerService userManagerService) : base(context, eventBus)
        {
            _userManagerService = userManagerService;
        }

        public async Task<Unit> Handle(CreateApplicationUserCommand request, CancellationToken cancellationToken)
        {
            var user = new ApplicationUser(
                request.Model.Id,
                ApplicationUserType.User,
                request.Model.Email,
                request.Model.FirstName,
                request.Model.LastName,
                request.Model.PhoneNumber
            );

            var userExist = await _userManagerService.UserExistsAsync(user.Email);
            if (userExist)
            {
                throw new DomainException("User exists");
            }

            var createResponse = await _userManagerService.CreateUserAsync(user, ApplicationUserType.User, request.Model.Password);

            if(!createResponse.Result.Succeeded)
            {
                throw new DomainException(createResponse.Result.Errors);
            }

            await SaveAndPublish(user, cancellationToken);

            return Unit.Value;
        }
    }
}
