﻿using QuoteQuiz.Common.Domain.ValueObjects;
using System;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.CreateApplicationUser
{
    public class CreateApplicationUserModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public PhoneNumber PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}
