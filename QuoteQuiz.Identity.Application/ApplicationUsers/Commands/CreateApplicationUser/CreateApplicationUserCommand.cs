﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.CreateApplicationUser
{
    public class CreateApplicationUserCommand : ICommand
    {
        public CreateApplicationUserModel Model { get; set; }
    }
}
