﻿using FluentValidation;

namespace QuoteQuiz.Identity.Application.ApplicationUsers.Commands.CreateApplicationUser
{
    public class CreateApplicationUserCommandValidator : AbstractValidator<CreateApplicationUserCommand>
    {
        public CreateApplicationUserCommandValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Id).NotNull();
            RuleFor(x => x.Model.Password).NotNull();
            RuleFor(x => x.Model.Email).NotNull().EmailAddress();
            RuleFor(x => x.Model.FirstName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();
            RuleFor(x => x.Model.LastName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();
            RuleFor(x => x.Model.Password).MinimumLength(6).MaximumLength(50).NotNull();

            RuleFor(x => x.Model.PhoneNumber).NotNull();
        }
    }
}
