﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Persistence.Context;
using Xunit;

namespace MRKT.Common.Persistence.Test.Context
{
    public class ApplicationDbContextTests
    {
        private readonly string _userId;
        private readonly DateTime _dateTime;

        private readonly Mock<IDateTimeService> _dateTimeMock;
        private readonly Mock<ICurrentUserService> _currentUserServiceMock;

        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IOptions<OperationalStoreOptions> _operationalStoreOptions;
        public ApplicationDbContextTests()
        {
            _dateTime = new DateTime(3001, 1, 1);
            _dateTimeMock = new Mock<IDateTimeService>();
            _dateTimeMock.Setup(m => m.Now).Returns(_dateTime);

            _userId = "00000000-0000-0000-0000-000000000000";
            _currentUserServiceMock = new Mock<ICurrentUserService>();
            _currentUserServiceMock.Setup(m => m.UserId).Returns(_userId);

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            _operationalStoreOptions = Options.Create(new OperationalStoreOptions());
            _applicationDbContext = new ApplicationDbContext(options, _operationalStoreOptions, _currentUserServiceMock.Object, _dateTimeMock.Object);
        }

        [Fact]
        public async Task SaveChangesAsyncShouldSetCreatedProperties()
        {
            var author = new Author(
                  Guid.NewGuid(),
                  "Test city"
            );

            _applicationDbContext.Set<Author>().Add(author);
            await _applicationDbContext.SaveChangesAsync();

            Assert.Null(author.LastModified);
            Assert.Equal(_dateTime, author.CreatedAt);
            Assert.Equal(_userId, author.LastModifiedBy);
        }
    }
}
