﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IdentityServer4.Models;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Persistence.Context;
using QuoteQuiz.Common.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;

namespace QuoteQuiz.Identity.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddIdentityInfrastructure(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
        {
            services.AddIdentityServer(options =>
            {
                options.IssuerUri = "https://myidentity.com";
            })
              .AddApiAuthorization<ApplicationUser, ApplicationDbContext>(options =>
              {
                  options.ApiResources = new ApiResourceCollection(new List<ApiResource>() {
                      new ApiResource("QuoteQuiz.Identity.LauncherAPI", "Identity.Api", new List<string>(){ "role", "admin", "user", }),
                      new ApiResource("QuoteQuiz.Quiz.LauncherAPI", "Quiz.Api", new List<string>(){ "role", "admin", "user", })
                  });

                  options.Clients.Add(new Client
                  {
                      AccessTokenType = AccessTokenType.Jwt,
                      AlwaysSendClientClaims = true,
                      UpdateAccessTokenClaimsOnRefresh = true,
                      AlwaysIncludeUserClaimsInIdToken = true,
                      AllowAccessTokensViaBrowser = true,
                      IncludeJwtId = true,
                      ClientId = "QuoteQuiz.Identity.Api",
                      ClientSecrets = { new Secret("API.Secret".Sha256()) },
                      AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                      AllowedScopes = { "QuoteQuiz.Identity.LauncherAPI", "QuoteQuiz.Quiz.LauncherAPI" }
                  });

                  options.Clients.Add(new Client
                  {
                      ClientId = "react_spa",
                      ClientName = "Angular SPA",
                      AllowedGrantTypes = GrantTypes.Implicit,
                      AllowedScopes = { "QuoteQuiz.Identity.LauncherAPI", "QuoteQuiz.Quiz.LauncherAPI" },
                      RedirectUris = { "http://localhost:4200/auth-callback" },
                      PostLogoutRedirectUris = { "http://localhost:4200/" },
                      AllowedCorsOrigins = { "http://localhost:4200" },
                      AllowAccessTokensViaBrowser = true,
                      AccessTokenLifetime = 3600
                  });
              })
            .AddSigningCredential(configuration.GetSection("SigninKeyCredentials"), environment, @"\QuoteQuiz.Identity.Launcher");

            return services;
        }
    }
}
