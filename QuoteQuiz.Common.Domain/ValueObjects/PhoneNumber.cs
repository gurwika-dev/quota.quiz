﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using QuoteQuiz.Common.Domain.Common.Concrete.ValueObjects;
using QuoteQuiz.Common.Domain.Exceptions;

namespace QuoteQuiz.Common.Domain.ValueObjects
{
    public class PhoneNumber : ValueObject
    {
        public string Number { get; private set; }

        private PhoneNumber() { }
        public static PhoneNumber For(string PhoneNumberString)
        {
            var PhoneNumber = new PhoneNumber();

            try
            {
                string pattern = @"^([0-9]{9,9})$";
                Match match = Regex.Match(PhoneNumberString, pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                {
                    throw new Exception();
                }

                PhoneNumber.Number = PhoneNumberString;
            }
            catch (Exception ex)
            {
                throw new InvalidPhoneNumberException(PhoneNumberString, ex);
            }

            return PhoneNumber;
        }

        public static implicit operator string(PhoneNumber PhoneNumber)
        {
            return PhoneNumber.ToString();
        }

        public static explicit operator PhoneNumber(string PhoneNumber)
        {
            return For(PhoneNumber);
        }

        public override string ToString()
        {
            return $"{Number}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Number;
        }
    }
}
