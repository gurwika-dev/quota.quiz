﻿namespace QuoteQuiz.Common.Domain.Common.Abstraction.Aggregates
{
    public interface IEventSourcedAggregate : IEventSourced, IAggregate
    {
    }
}
