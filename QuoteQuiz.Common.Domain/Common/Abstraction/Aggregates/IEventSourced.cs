﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace QuoteQuiz.Common.Domain.Common.Abstraction.Aggregates
{
    public interface IEventSourced
    {
        [JsonIgnore]
        Queue<IEvent> PendingEvents { get; }
    }
}
