﻿using MediatR;

namespace QuoteQuiz.Common.Domain.Common.Abstraction.Commands
{
    public interface ICommand : IRequest { }
}
