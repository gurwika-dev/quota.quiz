﻿using MediatR;

namespace QuoteQuiz.Common.Domain.Common.Abstraction.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
