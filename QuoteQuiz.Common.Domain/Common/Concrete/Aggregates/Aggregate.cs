﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Aggregates;
using System;
using System.Text.Json.Serialization;

namespace QuoteQuiz.Common.Domain.Common.Concrete.Aggregates
{
    public abstract class Aggregate : IAggregate
    {
        protected Aggregate() { }

        [JsonIgnore]
        public Guid Id { get; protected set; }
        [JsonIgnore]
        public DateTime CreatedAt { get; protected set; }
        [JsonIgnore]
        public string LastModifiedBy { get; protected set; }
        [JsonIgnore]
        public DateTime? LastModified { get; protected set; }
        [JsonIgnore]
        public DateTime? DeletedAt { get; protected set; }

        public void Delete()
        {
            DeletedAt = DateTime.Now;
        }

        public void UpdateAddedCredentials(DateTime createdAt, string lastModifiedBy)
        {
            CreatedAt = createdAt;
            LastModifiedBy = lastModifiedBy;
        }

        public void UpdateModifiedCredentials(DateTime lastModified, string lastModifiedBy)
        {
            LastModified = lastModified;
            LastModifiedBy = lastModifiedBy;
        }
    }
}
