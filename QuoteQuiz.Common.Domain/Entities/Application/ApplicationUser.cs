﻿
using Microsoft.AspNetCore.Identity;
using QuoteQuiz.Common.Domain.Common.Abstraction.Aggregates;
using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using QuoteQuiz.Common.Domain.Common.Concrete.Aggregates;
using QuoteQuiz.Common.Domain.Entities.Application.Events;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace QuoteQuiz.Common.Domain.Entities.Application
{
    public class ApplicationUser: IdentityUser, IEventSourced
    {
        public ApplicationUserType Type { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public bool Disabled { get; protected set; }
        public virtual ICollection<QuoteApplicationUser> QuoteQuotes { get; private set; }

        [JsonIgnore]
        public Queue<IEvent> PendingEvents { get; }

        public ApplicationUser()
        {
            QuoteQuotes = new HashSet<QuoteApplicationUser>();
            PendingEvents = new Queue<IEvent>();
        }

        public ApplicationUser(Guid id, ApplicationUserType type, string email, string firstName, string lastName, PhoneNumber phoneNumber)
        {
            Id = id.ToString();
            Type = type;
            Email = email;
            UserName = email;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            PendingEvents = new Queue<IEvent>();

            RiseEvent(
                new ApplicationUserCreatedEvent(
                    Id
                )
            );
        }

        public void Update(string firstName, string lastName, PhoneNumber phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;

            RiseEvent(
                new ApplicationUserUpdatedEvent(
                    Id
                )
            );
        }

        public void Disable()
        {
            Disabled = true;

            RiseEvent(
                new ApplicationUserDisabledEvent(
                    Id
                )
            );
        }

        public void Enable()
        {
            Disabled = false;

            RiseEvent(
                new ApplicationUserEnabledEvent(
                    Id
                )
            );
        }

        protected void RiseEvent(IEvent @event)
        {
            PendingEvents.Enqueue(@event);
        }
    }
}
