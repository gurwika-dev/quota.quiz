﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteQuiz.Common.Domain.Entities.Application.Events
{
    public class ApplicationUserEnabledEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public ApplicationUserEnabledEvent(
            string id,
            string payload = null
        )
        {
            Id = Guid.Parse(id);
            Payload = payload;
        }
    }
}
