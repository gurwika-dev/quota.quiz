﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;

namespace QuoteQuiz.Common.Domain.Entities.Application.Events
{
    public class ApplicationUserUpdatedEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public ApplicationUserUpdatedEvent(
            string id,
            string payload = null
        )
        {
            Id = Guid.Parse(id);
            Payload = payload;
        }
    }
}
