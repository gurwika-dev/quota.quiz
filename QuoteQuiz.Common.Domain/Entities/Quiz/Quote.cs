﻿using QuoteQuiz.Common.Domain.Common.Concrete.Aggregates;
using QuoteQuiz.Common.Domain.Entities.Quiz.Events;
using System;
using System.Collections.Generic;

namespace QuoteQuiz.Common.Domain.Entities.Quiz
{
    public class Quote : EventSourcedAggregate
    {
        public string Text { get; protected set; }
        public virtual Author Author { get; protected set; }
        public Guid AuthorId { get; set; } 
        public virtual ICollection<QuoteApplicationUser> QuoteQuotes { get; private set; }

        public Quote()
        {
            QuoteQuotes = new HashSet<QuoteApplicationUser>();
        }

        public Quote(Guid id, string text, Guid authorId)
        {
            Id = id;
            Text = text;
            AuthorId = authorId;

            RiseEvent(
                new QuoteCreatedEvent(
                    Id
                )
            );
        }

        public void Update(string text, Guid authorId)
        {
            Text = text;
            AuthorId = authorId;

            RiseEvent(
                new QuoteUpdateEvent(
                    Id
                )
            );
        }

        public new void Delete()
        {
            base.Delete();

            RiseEvent(
                new QuoteDeletedEvent(
                    Id
                )
            );
        }
    }
}
