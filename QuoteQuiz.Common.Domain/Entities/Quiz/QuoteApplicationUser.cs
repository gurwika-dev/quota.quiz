﻿using QuoteQuiz.Common.Domain.Common.Concrete.Aggregates;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Entities.Quiz.Events;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using System;

namespace QuoteQuiz.Common.Domain.Entities.Quiz
{
    public class QuoteApplicationUser : EventSourcedAggregate
    {
        public QuizeMode Mode { get; protected set; }
        public bool IsCorrect { get; protected set; }
        public string Answer { get; protected set; }
        public virtual Quote Quote { get; protected set; }
        public Guid QuoteId { get; protected set; }
        public virtual ApplicationUser ApplicationUser { get; protected set; }
        public string ApplicationUserId { get; protected set; }

        public QuoteApplicationUser()
        {
        }

        public QuoteApplicationUser(Guid id, QuizeMode mode, bool isCorrect, string answer, Guid quoteId, string applicationUserId)
        {
            Id = id;
            Mode = mode;
            Answer = answer;
            QuoteId = quoteId;
            ApplicationUserId = applicationUserId;
            IsCorrect = isCorrect;

            RiseEvent(
                new QuoteApplicationUserCreatedEvent(
                    Id
                )
            );
        }

        public QuoteApplicationUser(Guid id, QuizeMode mode, string applicationUserId, Quote quote, Author userAnswerAuthor, bool isAuthor)
        {
            Id = id;
            Mode = mode;
            ApplicationUserId = applicationUserId;
            QuoteId = quote.Id;

            if (mode == QuizeMode.Binary)
            {
                IsCorrect = (isAuthor && quote.AuthorId == userAnswerAuthor.Id) || (!isAuthor && quote.AuthorId != userAnswerAuthor.Id);
                Answer = isAuthor ? userAnswerAuthor.DisplayName : "Other";
            } else
            {
                IsCorrect = quote.AuthorId == userAnswerAuthor.Id;
                Answer = userAnswerAuthor.DisplayName;
            }

            RiseEvent(
                new QuoteApplicationUserCreatedEvent(
                    Id
                )
            );
        }
    }
}
