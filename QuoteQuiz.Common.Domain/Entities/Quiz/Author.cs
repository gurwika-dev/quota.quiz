﻿using QuoteQuiz.Common.Domain.Common.Concrete.Aggregates;
using System;
using System.Collections.Generic;

namespace QuoteQuiz.Common.Domain.Entities.Quiz
{
    public class Author : Aggregate
    {
        public string DisplayName { get; protected set; }
        public virtual ICollection<Quote> Quotes { get; private set; }

        public Author()
        {
            Quotes = new HashSet<Quote>();
        }

        public Author(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }

        public void Update(string displayName)
        {
            DisplayName = displayName;
        }
    }
}
