﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;

namespace QuoteQuiz.Common.Domain.Entities.Quiz.Events
{
    public class QuoteDeletedEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public QuoteDeletedEvent(
            Guid id
        )
        {
            Id = id;
        }
    }
}
