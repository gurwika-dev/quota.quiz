﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;

namespace QuoteQuiz.Common.Domain.Entities.Quiz.Events
{
    public class QuoteCreatedEvent: IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public QuoteCreatedEvent(
            Guid id,
            string payload = null
        )
        {
            Id = id;
            Payload = payload;
        }
    }
}
