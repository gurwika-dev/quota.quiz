﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System;

namespace QuoteQuiz.Common.Domain.Entities.Quiz.Events
{
    public class QuoteApplicationUserCreatedEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public QuoteApplicationUserCreatedEvent(
            Guid id,
            string payload = null
        )
        {
            Id = id;
            Payload = payload;
        }
    }
}

