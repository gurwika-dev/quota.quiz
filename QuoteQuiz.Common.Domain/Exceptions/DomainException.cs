﻿using System;

namespace QuoteQuiz.Common.Domain.Exceptions
{
    public class DomainException : Exception
    {
        public DomainException() { }
        public DomainException(string[] errors) : base(string.Join(", ", errors)) { }
        public DomainException(string message) : base(message) {  }
        public DomainException(string message, Exception ex) : base(message, ex) {  }
    }
}
