﻿namespace QuoteQuiz.Common.Domain.Enumarations.Application
{
    public enum QuizeMode
    {
        Binary,
        Multiple,
    }
}
