﻿namespace QuoteQuiz.Common.Domain.Enumarations.Application
{
    public enum ApplicationUserType
    {
        Admin,
        User
    }
}
