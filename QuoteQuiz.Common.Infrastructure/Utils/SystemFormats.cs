﻿namespace QuoteQuiz.Common.Infrastructure.Utils
{
    public class SystemFormats
    {
        public const string ShortDatePattern = "yyyy-MM-dd";
        public const string LongDatePattern = "yyyy-MM-ddTHH:mm:ss.FFFFFFFK";
        public const string DateShortTimePattern = "yyyy-MM-ddTHH:mm:ss";
        public const string DateTimeOffsetShortTimePattern = "yyyy-MM-ddTHH:mm:ss";
        public const string ShortTimePattern = "HH:mm";
        public const string LongTimePattern = "HH:mm:ss";
        public const string NumberDecimalSeparator = ".";
        public const string NumberGroupSeparator = " ";
        public const string DatePatternForMomentjs = "yyyy-MM-dd HH:mm:ss";
        public const string MomentToDateTimeOffset = "ddd MMM dd yyyy HH:mm:ss";
        public static string FullDateTimeOffsetPattern = "yyyy-MM-ddTHH:mm:ss.FFFFFFFK";
    }
}