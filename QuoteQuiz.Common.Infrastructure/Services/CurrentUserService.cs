﻿using System.Net;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using QuoteQuiz.Common.Application.Common.Abstraction;

namespace QuoteQuiz.Common.Infrastructure.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            Lang = httpContextAccessor.HttpContext?.Request?.Headers["Accept-Language"];
            Ip = httpContextAccessor.HttpContext?.Connection.RemoteIpAddress;
            UserId = httpContextAccessor.HttpContext?.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            IsAuthenticated = UserId != null;
        }

        public string Lang { get; }
        public IPAddress Ip { get; }
        public string UserId { get; }
        public bool IsAuthenticated { get; }
    }
}
