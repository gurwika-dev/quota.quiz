﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using QuoteQuiz.Common.Application.Common.Abstraction;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuoteQuiz.Common.Infrastructure.Services
{
    public class ProfileService : IProfileService
    {
        private readonly IUserManagerService _userManagerService;

        public ProfileService(IUserManagerService userManagerService)
        {
            _userManagerService = userManagerService;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _userManagerService.GetUserAsync(context.Subject);

            IList<string> roles = await _userManagerService.GetRolesAsync(user);

            IList<Claim> roleClaims = new List<Claim>();

            foreach (string role in roles)
            {
                roleClaims.Add(new Claim(JwtClaimTypes.Role, role));
            }

            context.IssuedClaims.AddRange(roleClaims);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.CompletedTask;
        }
    }
}
