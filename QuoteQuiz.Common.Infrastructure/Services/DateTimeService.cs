﻿using QuoteQuiz.Common.Application.Common.Abstraction;
using System;

namespace QuoteQuiz.Common.Infrastructure.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now => DateTime.Now;

        public int CurrentYear => DateTime.Now.Year;
    }
}
