﻿using Microsoft.AspNetCore.Identity;
using QuoteQuiz.Common.Application.Common.Models;
using System.Linq;

namespace QuoteQuiz.Common.Infrastructure.Extentions
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}
