﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Infrastructure.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace QuoteQuiz.Common.Infrastructure.Extentions
{
    public static class ControllersInfrastructureExtentions
    {
        public static IMvcBuilder AddControllersInfrastructure(this IMvcBuilder builder)
        {
            return builder
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.TypeNameHandling = TypeNameHandling.Objects;
                    options.SerializerSettings.DateFormatString = SystemFormats.ShortDatePattern;
                    options.SerializerSettings.Converters.Add(new IsoDateTimeConverter() { DateTimeFormat = SystemFormats.LongDatePattern });
                })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<IApplicationDbContext>());
        }
    }
}
