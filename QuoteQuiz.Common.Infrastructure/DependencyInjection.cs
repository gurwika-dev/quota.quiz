﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Infrastructure.Common;
using QuoteQuiz.Common.Infrastructure.Services;

namespace QuoteQuiz.Common.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommonInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEventBus, EventBus>();

            services.AddScoped<IUserManagerService, UserManagerService>();
            services.AddScoped<IUserRoleManagerService, UserRoleManagerService>();
            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddTransient<IDateTimeService, DateTimeService>();

            return services;
        }
    }
}
