﻿using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Concrete.Aggregates;
using QuoteQuiz.Common.Domain.Entities.Application;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Common.Application.Common.Handlers
{
    public abstract class CommandHandler
    {
        protected readonly IEventBus _eventBus;
        protected readonly IApplicationDbContext _context;

        public CommandHandler(IApplicationDbContext context, IEventBus eventBus)
        {
            _context = context;
            _eventBus = eventBus;
        }

        protected async Task SaveAndPublish(ApplicationUser eventSourcedAggregate, CancellationToken cancellationToken = default(CancellationToken))
        {
            await SaveChanges();

            await _eventBus.Publish(eventSourcedAggregate.PendingEvents.ToArray());
        }

        protected async Task SaveAndPublish(EventSourcedAggregate eventSourcedAggregate, CancellationToken cancellationToken = default(CancellationToken))
        {
            await SaveChanges();

            await _eventBus.Publish(eventSourcedAggregate.PendingEvents.ToArray());
        }

        protected async Task SaveChanges(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
