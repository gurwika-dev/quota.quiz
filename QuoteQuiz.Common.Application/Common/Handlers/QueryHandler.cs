﻿using AutoMapper;
using QuoteQuiz.Common.Application.Context.Abstraction;

namespace QuoteQuiz.Common.Application.Common.Handlers
{
    public class QueryHandler
    {
        protected readonly IApplicationDbContext _context;
        protected readonly IMapper _mapper;

        public QueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
