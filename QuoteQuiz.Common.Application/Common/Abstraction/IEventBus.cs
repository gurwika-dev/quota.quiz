﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Events;
using System.Threading.Tasks;

namespace QuoteQuiz.Common.Application.Common.Abstraction
{
    public interface IEventBus
    {
        Task Publish<TEvent>(params TEvent[] events) where TEvent : IEvent;
    }
}
