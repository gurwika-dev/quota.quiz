﻿using QuoteQuiz.Common.Application.Common.Models;
using System.Threading.Tasks;

namespace QuoteQuiz.Common.Application.Common.Abstraction
{
    public interface IUserRoleManagerService
    {
        Task<bool> RoleExistsAsync(string roleName);
        Task<(Result Result, string userRoleId)> CreateUserRoleAsync(string roleName);
        Task<Result> DeleteUserAsync(string userRoleId);
    }
}
