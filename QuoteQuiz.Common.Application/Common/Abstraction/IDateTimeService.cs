﻿using System;

namespace QuoteQuiz.Common.Application.Common.Abstraction
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}
