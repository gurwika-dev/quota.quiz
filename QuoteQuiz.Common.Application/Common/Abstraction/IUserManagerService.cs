﻿using QuoteQuiz.Common.Application.Common.Models;
using QuoteQuiz.Common.Domain.Entities.Application;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuoteQuiz.Common.Application.Common.Abstraction
{
    public interface IUserManagerService
    {
        Task<bool> UserExistsAsync(string userName);
        Task<(Result Result, string UserId)> CreateUserAsync(ApplicationUser user, ApplicationUserType type, string password);
        Task<Result> DeleteUserAsync(string userId);
        Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal);
        Task<IList<string>> GetRolesAsync(ApplicationUser user);
    }
}
