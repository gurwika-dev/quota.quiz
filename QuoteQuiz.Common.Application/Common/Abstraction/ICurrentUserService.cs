﻿using System.Net;

namespace QuoteQuiz.Common.Application.Common.Abstraction
{
    public interface ICurrentUserService
    {
        IPAddress Ip { get; }
        string UserId { get; }
        bool IsAuthenticated { get; }
    }
}
