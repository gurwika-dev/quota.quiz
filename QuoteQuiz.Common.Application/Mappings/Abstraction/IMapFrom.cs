﻿using AutoMapper;

namespace QuoteQuiz.Common.Application.Mappings.Abstraction
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
