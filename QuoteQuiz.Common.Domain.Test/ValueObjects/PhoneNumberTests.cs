﻿using QuoteQuiz.Common.Domain.Exceptions;
using QuoteQuiz.Common.Domain.ValueObjects;
using Xunit;

namespace MRKT.Common.Domain.Test.ValueObjects
{
    public class PhoneNumberTests
    {
        [Fact]
        public void ExplicitConversionFromStringToEmail()
        {
            var phoneNumberString = "577123456";
            var phoneNumber = (PhoneNumber)phoneNumberString;

            Assert.Equal(phoneNumberString, phoneNumber.ToString());
        }

        [Fact]
        public void ToStringShouldReturnCorrectEmail()
        {
            var phoneNumberString = "577123456";
            var phoneNumber = PhoneNumber.For(phoneNumberString);

            Assert.Equal(phoneNumberString, phoneNumber.ToString());
        }

        [Fact]
        public void ShouldThrow()
        {
            Assert.Throws<InvalidPhoneNumberException>(() => (PhoneNumber)"5771234569");
        }
    }
}
