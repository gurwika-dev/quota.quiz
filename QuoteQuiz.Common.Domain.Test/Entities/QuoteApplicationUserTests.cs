﻿using System;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Entities.Quiz.Events;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using Xunit;

namespace QuoteQuiz.Common.Domain.Test.Entities
{
    public class QuoteApplicationUserTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var mode = QuizeMode.Binary;
            var isCorrect = true;
            var answer = "Test answer";
            var quoteId = Guid.NewGuid();
            var applicationUserI = Guid.NewGuid().ToString();

            var quoteApplicationUser = new QuoteApplicationUser(
                id,
                mode,
                isCorrect,
                answer,
                quoteId,
                applicationUserI
            );

            Assert.Equal(id, quoteApplicationUser.Id);
            Assert.Equal(mode, quoteApplicationUser.Mode);
            Assert.Equal(isCorrect, quoteApplicationUser.IsCorrect);
            Assert.Equal(answer, quoteApplicationUser.Answer);
            Assert.Equal(quoteId, quoteApplicationUser.QuoteId);
            Assert.Equal(applicationUserI, quoteApplicationUser.ApplicationUserId);
            Assert.IsType<QuoteApplicationUserCreatedEvent>(quoteApplicationUser.PendingEvents.Peek());
        }
    }
}
