﻿using System;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Entities.Quiz.Events;
using Xunit;

namespace MRKT.Common.Domain.Test.Entities
{
    public class QuoteTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var text = "Test";
            var authorId = Guid.NewGuid();

            var quote = new Quote(
                id,
                text,
                authorId
            );

            Assert.Equal(id, quote.Id);
            Assert.Equal(text, quote.Text);
            Assert.Equal(authorId, quote.AuthorId);
            Assert.IsType<QuoteCreatedEvent>(quote.PendingEvents.Peek());
        }

        [Fact]
        public void ShouldMatchAllUpdatedFileds()
        {
            var id = Guid.NewGuid();
            var text = "Test";
            var authorId = Guid.NewGuid();

            var quote = new Quote(
                id,
                text,
                authorId
            );

            text = "Test : updated";
            authorId = Guid.NewGuid();

            quote.Update(text, authorId);

            Assert.Equal(id, quote.Id);
            Assert.Equal(text, quote.Text);
            Assert.Equal(authorId, quote.AuthorId);
            Assert.Equal(2, quote.PendingEvents.Count);
            Assert.IsType<QuoteUpdateEvent>(quote.PendingEvents.ToArray()[1]);
        }

        [Fact]
        public void ShouldDelete()
        {
            var id = Guid.NewGuid();
            var displayName = "Test";
            var authorId = Guid.NewGuid();

            var quote = new Quote(
                id,
                displayName,
                authorId
            );

            quote.Delete();

            Assert.NotNull(quote.DeletedAt);
            Assert.Equal(2, quote.PendingEvents.Count);
            Assert.IsType<QuoteDeletedEvent>(quote.PendingEvents.ToArray()[1]);
        }
    }
}
