﻿using System;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using Xunit;

namespace MRKT.Common.Domain.Test.Entities
{
    public class AuthorTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var displayName = "Author name";

            var author = new Author(
                id,
                displayName
            );

            Assert.Equal(id, author.Id);
            Assert.Equal(displayName, author.DisplayName);
        }

        [Fact]
        public void ShouldMatchAllUpdatedFileds()
        {
            var id = Guid.NewGuid();
            var displayName = "Author name";

            var author = new Author(
                id,
                displayName
            );

            displayName = "Author name : updated";
            author.Update(displayName);

            Assert.Equal(displayName, author.DisplayName);
        }

        [Fact]
        public void ShouldDelete()
        {
            var id = Guid.NewGuid();
            var displayName = "Author name";

            var author = new Author(
                id,
                displayName
            );

            author.Delete();

            Assert.NotNull(author.DeletedAt);
        }
    }
}
