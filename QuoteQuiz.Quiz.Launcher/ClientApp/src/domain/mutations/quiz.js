// Request fetch
export const REQUEST_FETCH_QUIZ = `REQUEST_FETCH_QUIZ`;
export const SUCCESS_FETCH_QUIZ = `SUCCESS_FETCH_QUIZ`;
export const FAILED_FETCH_QUIZ = `FAILED_FETCH_QUIZ`;

export const requestFetchQuiz = mode => ({
    type: REQUEST_FETCH_QUIZ,
    mode
});

export const successFetchQuiz = quiz => ({
    type: SUCCESS_FETCH_QUIZ,
    quiz
});

export const failedFetchQuiz = exception => ({
    type: FAILED_FETCH_QUIZ,
    exception
});

// Request fetch
export const REQUEST_ANSWER_QUIZ = `REQUEST_ANSWER_QUIZ`;
export const SUCCESS_ANSWER_QUIZ = `SUCCESS_ANSWER_QUIZ`;
export const FAILED_ANSWER_QUIZ = `FAILED_ANSWER_QUIZ`;

export const requestAnswerQuiz = answer => ({
    type: REQUEST_ANSWER_QUIZ,
    answer
});

export const successAnswerQuiz = () => ({
    type: SUCCESS_ANSWER_QUIZ
});

export const failedAnswerQuiz = exception => ({
    type: FAILED_ANSWER_QUIZ,
    exception
});
