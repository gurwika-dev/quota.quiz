// Init identity
export const INIT_IDENTITY = `INIT_IDENTITY`;
export const TOGGLE_EXPAND = `TOGGLE_EXPAND`;

export const initIdentity = identity => ({
    type: INIT_IDENTITY,
    identity
});

export const toggleExpand = () => ({
    type: TOGGLE_EXPAND
});

// Progress
export const REQUEST_CHANGE_MODE = `REQUEST_CHANGE_MODE`;

export const requestChangeMode = mode => ({
    type: REQUEST_CHANGE_MODE,
    mode
});

// Sign in
export const REQUEST_SIGN_IN = `REQUEST_SIGN_IN`;
export const FAILED_SIGN_IN = `FAILED_SIGN_IN`;
export const SUCCESS_SIGN_IN = `SUCCESS_SIGN_IN`;

export const requestSignIn = (userName, password) => ({
    type: REQUEST_SIGN_IN,
    userName,
    password
});

export const failedSignIn = exception => ({
    type: FAILED_SIGN_IN,
    exception
});

export const successSignIn = result => ({
    type: SUCCESS_SIGN_IN,
    result
});

// Sign up
export const REQUEST_SIGN_UP = `REQUEST_SIGN_UP`;
export const FAILED_SIGN_UP = `FAILED_SIGN_UP`;
export const SUCCESS_SIGN_UP = `SUCCESS_SIGN_UP`;

export const requestSignUp = user => ({
    type: REQUEST_SIGN_UP,
    user
});

export const failedSignUp = exception => ({
    type: FAILED_SIGN_UP,
    exception
});

export const successSignUp = () => ({
    type: SUCCESS_SIGN_UP
});

// Sign up
export const REQUEST_SIGN_OFF = `REQUEST_SIGN_OFF`;

export const requestSignOff = () => ({
    type: REQUEST_SIGN_OFF
});
