// Sign up
export const REQUEST_STATISTICS = `REQUEST_STATISTICS`;
export const FAILED_STATISTICS = `FAILED_STATISTICS`;
export const SUCCESS_STATISTICS = `SUCCESS_STATISTICS`;

export const requestStatistics = filter => ({
    type: REQUEST_STATISTICS,
    filter
});

export const failedStatistics = exception => ({
    type: FAILED_STATISTICS,
    exception
});

export const successStatistics = statistics => ({
    type: SUCCESS_STATISTICS,
    statistics
});
