// Request fetch
export const REQUEST_FETCH_QUOTES = `REQUEST_FETCH_QUOTES`;
export const SUCCESS_FETCH_QUOTES = `SUCCESS_FETCH_QUOTES`;
export const FAILED_FETCH_QUOTES = `FAILED_FETCH_QUOTES`;

export const requestFetchQuotes = filter => ({
    type: REQUEST_FETCH_QUOTES,
    filter
});

export const successFetchQuotes = quotes => ({
    type: SUCCESS_FETCH_QUOTES,
    quotes
});

export const failedFetchQuotes = exception => ({
    type: FAILED_FETCH_QUOTES,
    exception
});

// Request fetch
export const REQUEST_FETCH_QUOTE_ANSWER = `REQUEST_FETCH_QUOTE_ANSWER`;
export const SUCCESS_FETCH_QUOTE_ANSWER = `SUCCESS_FETCH_QUOTE_ANSWER`;
export const FAILED_FETCH_QUOTE_ANSWER = `FAILED_FETCH_QUOTE_ANSWER`;

export const requestFetchQuoteAnswer = (quoteId, authorId) => ({
    type: REQUEST_FETCH_QUOTE_ANSWER,
    quoteId,
    authorId
});

export const successFetchQuoteAnswer = quote => ({
    type: SUCCESS_FETCH_QUOTE_ANSWER,
    quote
});

export const failedFetchQuoteAnswer = exception => ({
    type: FAILED_FETCH_QUOTE_ANSWER,
    exception
});
