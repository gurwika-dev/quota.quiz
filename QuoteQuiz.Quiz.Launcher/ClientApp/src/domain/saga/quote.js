import { take, put } from "redux-saga/effects";

import * as mutations from "../mutations";
import httpService from "../../application/services/http-service";

export function* requestFetchQuoteAnswerSage() {
    while (true) {
        const { quoteId, authorId } = yield take(mutations.REQUEST_FETCH_QUOTE_ANSWER);

        try {
            const { data } = yield httpService.get(`api/quotes/${quoteId}/answer`, { timeout: 5000 });

            data.entry.isCorrect = data.entry.authorId === authorId;

            yield put(mutations.successFetchQuoteAnswer(data));
        } catch (e) {
            yield put(mutations.failedFetchQuoteAnswer(e));
        }
    }
}

export function* requestFetchQuotesSage() {
    while (true) {
        const { filter } = yield take(mutations.REQUEST_FETCH_QUOTES);

        try {
            const { data } = yield httpService.get(`api/quotes`, { params: filter, timeout: 5000 });

            yield put(mutations.successFetchQuotes(data));
        } catch (e) {
            yield put(mutations.failedFetchQuotes(e));
        }
    }
}
