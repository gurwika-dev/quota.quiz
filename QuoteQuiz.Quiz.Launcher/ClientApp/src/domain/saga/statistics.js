import { take, put } from "redux-saga/effects";

import * as mutations from "../mutations";
import httpService from "../../application/services/http-service";

export function* requestStatisticsSage() {
    while (true) {
        const { filter } = yield take(mutations.REQUEST_STATISTICS);

        try {
            const { data } = yield httpService.get(`api/quote-application-users/statistics`, { params: filter, timeout: 5000 });

            yield put(mutations.successStatistics(data));
        } catch (e) {
            yield put(mutations.failedStatistics(e));
        }
    }
}
