import { take, put } from "redux-saga/effects";

import * as mutations from "../mutations";
import { history } from "../../persistence/history/index";
import authenticationService from "../../application/services/authentication.service";

export function* requestSignOffSage() {
    while (true) {
        yield take(mutations.REQUEST_SIGN_OFF);
        yield authenticationService.signOff();
    }
}

export function* requestChangeModeSage() {
    while (true) {
        const { mode } = yield take(mutations.REQUEST_CHANGE_MODE);
        yield authenticationService.setMode({ mode });
    }
}

export function* requestSingInSage() {
    while (true) {
        const { userName, password } = yield take(mutations.REQUEST_SIGN_IN);

        try {
            const { data } = yield authenticationService.singIn(userName, password);
            authenticationService.setIdentity({ isAuthenticated: true, accessToken: data.access_token });
            yield put(mutations.successSignIn(data));
            history.push(`/`);
        } catch (e) {
            yield put(mutations.failedSignIn(e));
        }
    }
}
