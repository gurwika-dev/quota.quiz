import { take, put } from "redux-saga/effects";

import * as mutations from "../mutations";
import httpService from "../../application/services/http-service";

export function* requestFetchQuizSage() {
    while (true) {
        const { mode } = yield take(mutations.REQUEST_FETCH_QUIZ);

        try {
            const { data } = yield httpService.get(`api/quote-application-users`, { params: { mode }, timeout: 5000 });

            yield put(mutations.successFetchQuiz(data));
        } catch (e) {
            yield put(mutations.failedFetchQuiz(e));
        }
    }
}

export function* requestAnswerQuizSage() {
    while (true) {
        const { answer } = yield take(mutations.REQUEST_ANSWER_QUIZ);

        try {
            yield httpService.post(`api/quote-application-users`, answer, { timeout: 5000 });

            yield put(mutations.successAnswerQuiz());
        } catch (e) {
            yield put(mutations.failedAnswerQuiz(e));
        }
    }
}
