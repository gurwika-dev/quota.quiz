import * as mutations from "../mutations";

export default (
    state = { totalCount: 0, entries: [], percentageOfBinaryAnswers: null, percentageOfCorrectAnswers: null } || {},
    action
) => {
    switch (action.type) {
        case mutations.SUCCESS_STATISTICS:
            return { ...state, ...action.statistics };
        default:
            return state;
    }
};
