import * as mutations from "../mutations";

import { QuizMode } from "../../application/types/quize-mode";

export default (
    state = { mode: QuizMode.Binary, accessToken: null, isAuthenticated: false, isLoading: true, expanded: false } || {},
    action
) => {
    switch (action.type) {
        case mutations.INIT_IDENTITY:
            return { ...state, ...action.identity };
        case mutations.TOGGLE_EXPAND:
            return { ...state, expanded: !state.expanded };
        case mutations.REQUEST_CHANGE_MODE:
            return { ...state, mode: action.mode };
        default:
            return state;
    }
};
