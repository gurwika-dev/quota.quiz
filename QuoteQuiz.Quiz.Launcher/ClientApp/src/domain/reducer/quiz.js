import * as mutations from "../mutations";

export default (state = { entry: null, isLoading: true } || {}, action) => {
    switch (action.type) {
        case mutations.REQUEST_FETCH_QUIZ:
            return { ...state, isLoading: true };
        case mutations.SUCCESS_FETCH_QUIZ:
            return { ...state, ...action.quiz, isLoading: false };
        case mutations.FAILED_FETCH_QUIZ:
            return { ...state, entry: null, isLoading: false };
        default:
            return state;
    }
};
