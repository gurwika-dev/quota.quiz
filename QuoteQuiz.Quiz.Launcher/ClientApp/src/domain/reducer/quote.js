import * as mutations from "../mutations";

export default (state = { totalCount: 0, entries: [], entry: null } || {}, action) => {
    switch (action.type) {
        case mutations.SUCCESS_FETCH_QUOTE_ANSWER:
            return { ...state, ...action.quote };
        case mutations.SUCCESS_FETCH_QUOTES:
            return { ...state, ...action.quotes };
        case mutations.REQUEST_FETCH_QUIZ:
            return { ...state, entry: null };
        default:
            return state;
    }
};
