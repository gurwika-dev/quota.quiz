import { combineReducers } from "redux";

import identity from "./identity";
import statistics from "./statistics";
import quiz from "./quiz";
import quote from "./quote";

export const reducer = combineReducers({
    identity,
    statistics,
    quiz,
    quote
});
