import React from "react";
import ReactDOM from "react-dom";
import App from "./presentation/App";

import serviceWorker from "./application/workers/service-worker";
import authenticationService from "./application/services/authentication.service";
import httpService from "./application/services/http-service";
import { ReduxProvider } from "./application/providers/redux-provider";

httpService.init({
    apiDomain: process.env.REACT_APP_API_DOMAIN,
    identityDomain: process.env.REACT_APP_IDENTITY_DOMAIN
});

ReactDOM.render(
    <ReduxProvider>
        <App />
    </ReduxProvider>,
    document.getElementById("root")
);

authenticationService.init({
    apiDomain: process.env.REACT_APP_API_DOMAIN,
    identityDomain: process.env.REACT_APP_IDENTITY_DOMAIN,
    clientId: process.env.REACT_APP_CLIENT_ID,
    clientSecret: process.env.REACT_APP_CLIENT_SECRET,
    redirect_uri: window.location.origin
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
