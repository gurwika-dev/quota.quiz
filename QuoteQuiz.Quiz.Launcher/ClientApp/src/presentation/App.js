import React from "react";
import { Route, Router } from "react-router-dom";

import { history } from "../persistence/history";

import PrivateRoute from "../application/guards/private-route";

import Dashboard from "./views/dashboard/Dashboard";
import Auth from "./views/auth/Auth";

import "./App.scss";

function App() {
    return (
        <Router history={history}>
            <PrivateRoute path="/" component={Dashboard} />
            <Route path="/auth" component={Auth} />
        </Router>
    );
}

export default App;
