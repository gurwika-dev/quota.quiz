import React from 'react';
import { Field } from 'formik';
import classNames from 'classnames';

import './TextBox.scss';

export default ({ name, label, type, placeholder, lessSpace, touched, error }) => (
	<div className="text-box">
		<div
			className={classNames({
				'text-box__wrapper': true,
				'text-box__wrapper--less-space': lessSpace
			})}
		>
			{label && (
				<div className="text-box__label">
					<label htmlFor={name}>{label}</label>
				</div>
			)}
			<div className="text-box__input">
				<div className="text-box__input-wrapper">
					<div
						className={classNames({
							'text-box__input-holder': true,
							'text-box__input-holder--error': error && touched,
							'text-box__input-holder--success': !error && touched
						})}
					>
						<Field name={name} type={type} placeholder={placeholder} autoComplete="off" className="text-box__input-field" />

						<div className="text-box__input-status">
							{error && touched ? (
								<svg className="text-box__input-status-error" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
									<path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path>
								</svg>
							) : null}
							{!error && touched ? (
								<svg className="text-box__input-status-ok" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
									<path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
								</svg>
							) : null}
						</div>
						<div className="text-box__input-line"></div>
					</div>
					{error && touched && <p className="text-box__input-error">{error}</p>}
				</div>
			</div>
		</div>
	</div>
);
