import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { Formik, Form } from 'formik';

import TextBox from './TextBox';
import { history } from '../../../persistence/history/index';

test('renders learn react link', () => {
	const { getByText, getByPlaceholderText } = render(
		<Router history={history}>
			<Formik>
				{() => (
					<Form>
						<TextBox name="email" type="text" label="Business email" placeholder="Enter email" error="Email is required" touched={true} />
					</Form>
				)}
			</Formik>
		</Router>
	);

	expect(getByText(/Business email/i)).toBeInTheDocument();
	expect(getByPlaceholderText(/Enter email/i)).toBeInTheDocument();
	expect(getByText(/Email is required/i)).toBeInTheDocument();
});
