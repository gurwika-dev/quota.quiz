import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";

import SlideBar from "./SlideBar";
import { history } from "../../../persistence/history/index";

import { QuizMode } from "../../../application/types/quize-mode";

test("renders learn react link", () => {
    const { getByRole } = render(
        <Router history={history}>
            <SlideBar mode={QuizMode.Binary} />
        </Router>
    );

    expect(getByRole(/menubar/i)).toBeInTheDocument();
    expect(getByRole(/heading/i)).toBeInTheDocument();
});
