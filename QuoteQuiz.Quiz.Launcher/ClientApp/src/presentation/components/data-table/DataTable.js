import React from "react";
import * as yup from "yup";
import PropTypes from "prop-types";
import { withFormik, Form } from "formik";

import TextBox from "../text-box/TextBox";
import SelectBox from "../select-box/SelectBox";
import Pagination from "../pagination/Pagination";

import "./DataTable.scss";

function DataTableFilter({ filter, pageSizeList, onChange }) {
    return (
        <Form onChange={event => onChange({ ...filter, keyword: event.target.value })}>
            <div className="mrk-container-fluid">
                <div className="mrk-row">
                    <div className="mrk-sm-3">
                        <SelectBox
                            name="limit"
                            placeholder="Per page"
                            options={pageSizeList}
                            lessSpace={true}
                            onChange={limit => {
                                onChange({ ...filter, limit });
                            }}
                        ></SelectBox>
                    </div>
                    <div className="mrk-sm-4"></div>
                    <div className="mrk-sm-5">
                        <TextBox
                            name="keyword"
                            type="text"
                            label="Filter"
                            lessSpace={true}
                            onChange={keyword => {
                                onChange({ ...filter, keyword });
                            }}
                        />
                    </div>
                </div>
            </div>
        </Form>
    );
}

const DataTableFilterFormik = withFormik({
    validationSchema: yup.object().shape({
        page: yup.number(),
        keyword: yup.string(),
        limit: yup.number()
    }),
    mapPropsToValues: ({ filter: { page, limit, keyword } }) => {
        return {
            page: page || "",
            limit: limit || "",
            keyword: keyword || ""
        };
    }
})(DataTableFilter);

function DataTable({ headers, pageSizeList, component: Component, filter, data: { totalCount, entries }, onChange, onDelete }) {
    const totalPages = Math.ceil(totalCount / filter.limit);

    return (
        <div className="data-table">
            <div className="data-table__container">
                <div className="data-table__wrapper">
                    <div className="data-table__filters">
                        <DataTableFilterFormik filter={filter} pageSizeList={pageSizeList} onChange={onChange} />
                    </div>

                    <table>
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                {headers.map(header => (
                                    <th scope="col" key={header}>
                                        {header}
                                    </th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            {entries.length === 0 ? (
                                <tr>
                                    <td colSpan={headers.length + 1}>No result</td>
                                </tr>
                            ) : null}
                            {entries.map((entry, index) => (
                                <Component entry={entry} key={entry.id} index={++index} onDelete={onDelete} />
                            ))}
                        </tbody>
                    </table>

                    <div className="data-table__pagination">
                        <div className="mrk-container-fluid">
                            <div className="mrk-row">
                                <div className="mrk-sm-6">
                                    <div className="data-table__pagination-info">
                                        Showing {filter.page} to {totalPages} of {totalCount} entries
                                    </div>
                                </div>
                                <div className="mrk-sm-6">
                                    <Pagination
                                        totalCount={totalCount}
                                        limit={filter.limit}
                                        page={filter.page}
                                        onChange={page => {
                                            onChange({ ...filter, page });
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

DataTable.defaultProps = {
    pageSizeList: [
        { label: "10", value: 10 },
        { label: "50", value: 50 },
        { label: "100", value: 100 }
    ]
};

DataTable.propTypes = {
    data: PropTypes.shape({
        totalCount: PropTypes.number.isRequired,
        entries: PropTypes.arrayOf(PropTypes.object),
        filter: PropTypes.object
    }).isRequired,
    headers: PropTypes.arrayOf(PropTypes.string).isRequired,
    pageSizeList: PropTypes.arrayOf(PropTypes.object),
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
    onChange: PropTypes.func.isRequired
};

export default DataTable;
