import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";

import DataTable from "./DataTable";
import { history } from "../../../persistence/history/index";

test("renders learn react link", () => {
    const headers = ["Name"];
    const filter = { limit: 10, page: 1 };
    const data = { totalCount: 10, entries: [] };

    const { getByText } = render(
        <Router history={history}>
            <DataTable headers={headers} filter={filter} data={data} component={() => {}} onChange={filter => {}} />
        </Router>
    );
});
