import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';

import Loading from './Loading';
import { history } from '../../../persistence/history/index';

test('renders learn react link', () => {
	const { getByRole } = render(
		<Router history={history}>
			<Loading />
		</Router>
	);
	const linkElement = getByRole(/progressbar/i);
	expect(linkElement).toBeInTheDocument();
});
