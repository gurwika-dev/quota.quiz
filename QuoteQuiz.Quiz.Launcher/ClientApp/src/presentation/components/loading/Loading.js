import React from "react";

import "./Loading.scss";

const Loading = () => <div className="loader" role="progressbar"></div>;

export default Loading;
