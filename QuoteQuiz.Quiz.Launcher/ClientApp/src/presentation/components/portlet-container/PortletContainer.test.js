import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";

import PortletContainer from "./PortletContainer";
import { history } from "../../../persistence/history/index";

test("renders learn react link", () => {
    const { getByRole } = render(
        <Router history={history}>
            <PortletContainer />
        </Router>
    );

    expect(getByRole(/menubar/i)).toBeInTheDocument();
});
