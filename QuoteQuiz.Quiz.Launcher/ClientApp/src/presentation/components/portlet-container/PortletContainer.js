import React from "react";

import "./PortletContainer.scss";

export default ({ header, children, dialog, showDialog }) => (
    <section className="portlet-container">
        <div className="portlet-container__wrapper">
            <div className="portlet-container__header">{header}</div>
            <div className="portlet-container__body" role="menubar">
                <div className="portlet-container__inner" style={{ transform: `translate(${showDialog ? -100 : 0}%, 0px)` }}>
                    <div className="portlet-container__content">{children}</div>
                    <div className="portlet-container__dialog">{dialog}</div>
                </div>
            </div>
        </div>
    </section>
);
