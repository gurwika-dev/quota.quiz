import React from "react";
import { NavLink } from "react-router-dom";
import classNames from "classnames";

import { store } from "../../../persistence/story";
import * as mutations from "../../../domain/mutations";

import "./Navigation.scss";

function Navigation({ expanded }) {
    return (
        <div
            className={classNames({
                navigation: true,
                "navigation--in": expanded
            })}
        >
            <div className="navigation__wrapper">
                <div className="navigation__container">
                    <div className="navigation__body">
                        <div className="navigation__profile">
                            <div className="navigation__profile-avatar">
                                <img src="/assets/icons/avatar.png" alt="Unknown" />
                            </div>
                            <ul role="heading">
                                <li>
                                    <div className="navigation__profile-toggle">
                                        <div className="navigation__profile-toggle-wrapper">Unknown</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <ul className="navigation__list" role="menubar">
                            <li>
                                <NavLink className="app-brand__link" to="/">
                                    <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                                        <path d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z"></path>
                                    </svg>
                                    <div>Dashboard</div>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="app-brand__link" to="/statistics">
                                    <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                                        <path d="M0 0h24v24H0z" fill="none" />
                                        <circle cx="7.2" cy="14.4" r="3.2" />
                                        <circle cx="14.8" cy="18" r="2" />
                                        <circle cx="15.2" cy="8.8" r="4.8" />
                                    </svg>
                                    <div>Statistics</div>
                                </NavLink>
                            </li>
                            <li>
                                <div
                                    onClick={() => {
                                        store.dispatch(mutations.requestSignOff());
                                    }}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                        <path d="M21.71 11.29l-9-9c-.39-.39-1.02-.39-1.41 0l-9 9c-.39.39-.39 1.02 0 1.41l9 9c.39.39 1.02.39 1.41 0l9-9c.39-.38.39-1.01 0-1.41zM14 14.5V12h-4v3H8v-4c0-.55.45-1 1-1h5V7.5l3.5 3.5-3.5 3.5z" />
                                        <path d="M0 0h24v24H0z" fill="none" />
                                    </svg>
                                    <div>Sign off</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div
                className="navigation__layer"
                onClick={() => {
                    store.dispatch(mutations.requestSignOff());
                }}
            ></div>
        </div>
    );
}

export default Navigation;
