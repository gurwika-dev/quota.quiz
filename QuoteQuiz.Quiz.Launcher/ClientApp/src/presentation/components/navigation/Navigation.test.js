import React from 'react';
import { render } from '@testing-library/react';
import { Router } from 'react-router-dom';

import Navigation from './Navigation';
import { history } from '../../../persistence/history/index';

test('renders learn react link', () => {
	const { getByRole } = render(
		<Router history={history}>
			<Navigation />
		</Router>
	);

	expect(getByRole(/menubar/i)).toBeInTheDocument();
	expect(getByRole(/heading/i)).toBeInTheDocument();
});
