import React from "react";
import PropTypes from "prop-types";

import PaginationService from "../../../application/services/pagination-service";
import { PaginationType } from "../../../application/types/pagination-type";

import "./Pagination.scss";

function Pagination({ page, totalCount, limit, onChange }) {
    var paginationList = PaginationService.getPagination(page, totalCount, limit);

    return (
        <div className="pagination">
            <div className="pagination__wrapper">
                <ul>
                    {paginationList.map(item => {
                        if (item.type === PaginationType.PrevBlacked) {
                            return (
                                <li className="pagination__prev pagination__disabled" key={`${item.type}-${item.index}`}>
                                    <span>Previous</span>
                                </li>
                            );
                        }
                        if (item.type === PaginationType.Prev) {
                            return (
                                <li
                                    className="pagination__prev clickable"
                                    key={`${item.type}-${item.index}`}
                                    onClick={() => onChange(item.index)}
                                >
                                    <span>Previous</span>
                                </li>
                            );
                        }
                        if (item.type === PaginationType.NextBlocked) {
                            return (
                                <li className="pagination__next pagination__disabled" key={`${item.type}-${item.index}`}>
                                    <span>Next</span>
                                </li>
                            );
                        }
                        if (item.type === PaginationType.Next) {
                            return (
                                <li
                                    className="pagination__next clickable"
                                    key={`${item.type}-${item.index}`}
                                    onClick={() => onChange(item.index)}
                                >
                                    <span>Next</span>
                                </li>
                            );
                        }
                        if (item.type === PaginationType.Dots) {
                            return (
                                <li key={`${item.type}-${item.index}`}>
                                    <span>...</span>
                                </li>
                            );
                        }
                        if (item.type === PaginationType.Active) {
                            return (
                                <li className="pagination__active" key={`${item.type}-${item.index}`}>
                                    <span>{item.index}</span>
                                </li>
                            );
                        }

                        return (
                            <li className="clickable" key={`${item.type}-${item.index}`} onClick={() => onChange(item.index)}>
                                <span>{item.index}</span>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
}

Pagination.propTypes = {
    page: PropTypes.number.isRequired,
    totalCount: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

export default Pagination;
