import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";

import Pagination from "./Pagination";
import { history } from "../../../persistence/history/index";

test("renders learn react link", () => {
    const { getByRole } = render(
        <Router history={history}>
            <Pagination totalCount={100} limit={10} page={2} onChange={page => {}} />
        </Router>
    );
});
