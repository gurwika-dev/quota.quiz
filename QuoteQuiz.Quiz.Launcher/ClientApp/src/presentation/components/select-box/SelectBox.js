import React from 'react';
import { Field } from 'formik';
import classNames from 'classnames';

import './SelectBox.scss';

export default class SelectBox extends React.PureComponent {
	constructor() {
		super();
		this.state = { isOpen: false };
	}

	render() {
		const { name, label, placeholder, options, lessSpace, onChange, touched, error } = this.props;

		return (
			<div className="select-box">
				<div
					className={classNames({
						'select-box__wrapper': true,
						'select-box__wrapper--less-space': lessSpace
					})}
				>
					{label && (
						<div className="select-box__label">
							<label htmlFor={name}>{label}</label>
						</div>
					)}
					<div className="select-box__input">
						<div className="select-box__input-wrapper">
							<Field
								name={name}
								component={({ field: { name, value }, form: { setFieldTouched, setFieldValue, ...rest } }) => (
									<div
										className={classNames({
											'select-box__input-holder': true,
											'select-box__input-holder--touched': touched,
											'select-box__input-holder--in': this.state.isOpen || !!value,
											'select-box__input-holder--open': this.state.isOpen,
											'select-box__input-holder--error': error && touched,
											'select-box__input-holder--success': !error && touched
										})}
									>
										<div className="select-box__input-placeholder">{placeholder}</div>
										<div className="select-box__input-container">
											<div className="select-box__input-field">
												<Field name={name} type="hidden" />
												<div
													onClick={() => {
														this.setState({ ...this.state, isOpen: true });
													}}
												>
													<span>{value ? options.find(item => item.value === value).label : null}</span>
												</div>
												<svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
													<path d="M7 10l5 5 5-5z"></path>
												</svg>
											</div>
											<div className="select-box__input-options">
												<ul>
													<li className="select-box__input-options--disabled">{placeholder}</li>
													{options.map(item => (
														<li
															className={classNames({
																'select-box__input-options--selected': item.value === value
															})}
															key={item.value}
															onClick={() => {
																setFieldTouched(`${name}`);
																setFieldValue(`${name}`, item.value);
																onChange && onChange(item.value);
																this.setState({ ...this.state, isOpen: false });
															}}
														>
															{item.label}
														</li>
													))}
												</ul>
											</div>
											<div
												className="select-box__input-overlay"
												onClick={() => {
													this.setState({ ...this.state, isOpen: false });
												}}
											></div>
										</div>

										<div className="select-box__input-line"></div>
									</div>
								)}
							/>
							{error && touched && <p className="select-box__input-error">{error}</p>}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
