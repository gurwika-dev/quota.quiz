import React from "react";
import { render } from "@testing-library/react";
import { Router } from "react-router-dom";
import { Formik, Form } from "formik";

import SelectBox from "./SelectBox";
import { history } from "../../../persistence/history/index";

test("renders learn react link", () => {
    const { getByText } = render(
        <Router history={history}>
            <Formik>
                {() => (
                    <Form>
                        <SelectBox
                            name="country"
                            label="Business types"
                            options={[
                                { label: "Project Groups", value: 10 },
                                { label: "Tutorial Groups", value: 50 }
                            ]}
                            error="Description is required"
                            touched={true}
                        ></SelectBox>
                    </Form>
                )}
            </Formik>
        </Router>
    );

    expect(getByText(/Business types/i)).toBeInTheDocument();
    expect(getByText(/Project Groups/i)).toBeInTheDocument();
    expect(getByText(/Tutorial Groups/i)).toBeInTheDocument();
});
