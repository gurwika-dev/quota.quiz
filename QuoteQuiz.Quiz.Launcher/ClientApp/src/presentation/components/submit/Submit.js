import React from 'react';

import './Submit.scss';

export default ({ title, disabled }) => (
	<div className="submit">
		<div className="submit__wrapper">
			<button className="submit__button" type="submit" disabled={disabled}>
				{title}
			</button>
		</div>
	</div>
);
