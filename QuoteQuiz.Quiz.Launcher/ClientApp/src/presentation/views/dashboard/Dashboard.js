import React from "react";
import { connect } from "react-redux";
import { Route, Switch, Redirect } from "react-router-dom";
import { createSelector } from "reselect";

import Statistics from "./statistics/Statistics";
import Quotes from "./quotes/Quotes";
import Quiz from "./quiz/Quiz";

import Navigation from "../../components/navigation/Navigation";
import SlideBar from "../../components/slide-Bar/SlideBar";

import "./Dashboard.scss";

function Dashboard({ expanded, mode }) {
    return (
        <div className="dashbaord">
            <div className="dashbaord__container">
                <div className="dashbaord__navigation">
                    <Navigation expanded={expanded} />
                </div>
                <div className="dashbaord__body">
                    <div className="dashbaord__body-wrapper">
                        <Switch>
                            <Route exact path="/" component={Quotes} />
                            <Route path="/statistics" component={Statistics} />
                            <Route path="/quiz" component={Quiz} />
                            <Redirect from="/*" to="/" />
                        </Switch>
                    </div>
                </div>
                <SlideBar mode={mode} />
            </div>
        </div>
    );
}

const getState = createSelector(
    state => state.identity,
    ({ expanded, mode }) => ({
        expanded,
        mode
    })
);

const mapStateToProps = state => {
    return getState(state);
};

export default connect(mapStateToProps)(Dashboard);
