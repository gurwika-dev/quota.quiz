import React from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";

import { store } from "../../../../persistence/story";
import * as mutations from "../../../../domain/mutations";

import DataTable from "../../../components/data-table/DataTable";
import PortletContainer from "../../../components/portlet-container/PortletContainer";

import Entry from "./entry/Entry";
import "./Quotes.scss";

class Quotes extends React.PureComponent {
    constructor() {
        super();
        this.state = { page: 1, limit: 10 };
    }

    componentDidMount() {
        this.handleChange(this.state);
    }

    handleChange = filter => {
        this.setState(filter);
        store.dispatch(mutations.requestFetchQuotes(filter));
    };

    render() {
        const { totalCount, entries } = this.props;

        return (
            <div className="quotes">
                <div className="mrk-container-fluid">
                    <PortletContainer>
                        <DataTable
                            headers={["Quote", "Number of Quizzes"]}
                            filter={this.state}
                            data={{ totalCount, entries }}
                            component={Entry}
                            onChange={filter => {
                                this.handleChange(filter);
                            }}
                        />
                    </PortletContainer>
                </div>
            </div>
        );
    }
}

const getState = createSelector(
    state => state.quote,
    ({ totalCount, entries }) => ({
        totalCount,
        entries
    })
);

const mapStateToProps = state => {
    return getState(state);
};

export default connect(mapStateToProps)(Quotes);
