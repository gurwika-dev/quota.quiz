import React from "react";

function Entry({ entry, index }) {
    return (
        <tr className="odd">
            <td>{index}</td>
            <td>{entry.text}</td>
            <td>{entry.numberOfQuizzes}</td>
        </tr>
    );
}

export default Entry;
