import React from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import * as uuidv4 from "uuid/v4";

import { store } from "../../../../persistence/story";
import * as mutations from "../../../../domain/mutations";

import Loading from "../../../../presentation/components/loading/Loading";
import PortletContainer from "../../../components/portlet-container/PortletContainer";

import { isNotNilOrEmpty } from "../../../../application/utils/is-not-nil-or-empty";
import { isNilOrEmpty } from "../../../../application/utils/is-nil-or-empty";

import "./Quiz.scss";

class Quiz extends React.PureComponent {
    componentDidMount() {
        this.handleNextQuiz();
    }

    handleNextQuiz = () => {
        const { mode } = this.props;
        store.dispatch(mutations.requestFetchQuiz(mode));
    };

    handleAnswer = (authorId, isAuthor) => {
        const {
            mode,
            entry: { quoteId }
        } = this.props;

        store.dispatch(mutations.requestFetchQuoteAnswer(quoteId, isAuthor ? authorId : null));
        store.dispatch(mutations.requestAnswerQuiz({ id: uuidv4(), mode, quoteId, authorId, isAuthor }));
    };

    render() {
        const { entry, isLoading, answer } = this.props;

        return (
            <div className="quiz">
                <div className="quiz__contaienr">
                    <div className="mrk-container-fluid">
                        {isLoading && <Loading />}
                        {!isLoading && (
                            <div className="mrk-row">
                                <div className="mrk-sm-8 mrk-sm-space-left-2">
                                    {isNilOrEmpty(entry) && <div className="quiz__empty">Quote was not found</div>}
                                    {isNotNilOrEmpty(entry) && (
                                        <PortletContainer>
                                            <div className="quiz__wrapper">
                                                <h2 className="quiz__quiestion">Who said it?</h2>
                                                <h2 className="quiz__quote">{entry.text}</h2>
                                                {entry.answers.length === 1 && (
                                                    <h2 className="quiz__author">- {entry.answers[0].displayName}</h2>
                                                )}
                                                {isNilOrEmpty(answer) && entry.answers.length > 1 && (
                                                    <div>
                                                        {entry.answers.map(answer => (
                                                            <button
                                                                key={answer.id}
                                                                className="quiz__answer"
                                                                onClick={() => this.handleAnswer(answer.id, true)}
                                                            >
                                                                {answer.displayName}
                                                            </button>
                                                        ))}
                                                    </div>
                                                )}
                                                {isNilOrEmpty(answer) && entry.answers.length === 1 && (
                                                    <div className="mrk-row">
                                                        <div className="mrk-sm-4">
                                                            <button
                                                                className="quiz__submit quiz__submit--yes"
                                                                onClick={() => this.handleAnswer(entry.answers[0].id, true)}
                                                            >
                                                                Yes
                                                            </button>
                                                        </div>
                                                        <div className="mrk-sm-4"></div>
                                                        <div className="mrk-sm-4">
                                                            <button
                                                                className="quiz__submit quiz__submit--no"
                                                                onClick={() => this.handleAnswer(entry.answers[0].id, false)}
                                                            >
                                                                No
                                                            </button>
                                                        </div>
                                                    </div>
                                                )}
                                                {isNotNilOrEmpty(answer) && (
                                                    <div className="quiz__correct-answer">
                                                        <div className=" mrk-row">
                                                            <div className="mrk-sm-8">
                                                                {answer.isCorrect && (
                                                                    <span>Correct! The right answer is: {answer.authorName}</span>
                                                                )}
                                                                {!answer.isCorrect && (
                                                                    <span>
                                                                        “Sorry, you are wrong! The right answer is: {answer.authorName}
                                                                    </span>
                                                                )}
                                                            </div>
                                                            <div className="mrk-sm-4">
                                                                <button
                                                                    className="quiz__submit quiz__submit--no"
                                                                    onClick={() => this.handleNextQuiz()}
                                                                >
                                                                    Next
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )}
                                            </div>
                                        </PortletContainer>
                                    )}
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

const getState = createSelector(
    state => state.identity,
    state => state.quiz,
    state => state.quote,
    ({ mode }, { entry, isLoading }, { entry: answer }) => ({ mode, entry, isLoading, answer })
);

const mapStateToProps = state => {
    return getState(state);
};

export default connect(mapStateToProps)(Quiz);
