import React from "react";

function Entry({ entry, index }) {
    return (
        <tr className="odd">
            <td>{index}</td>
            <td>{entry.answer}</td>
            <td>
                {entry.isCorrect ? (
                    <button className="data-table__label data-table__label--success" type="button">
                        <span>
                            <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                                <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                            </svg>
                            Correct
                        </span>
                    </button>
                ) : (
                    <button className="data-table__label data-table__label--warning" type="button">
                        <span>
                            <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                                <path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"></path>
                            </svg>
                            Incorrect
                        </span>
                    </button>
                )}
            </td>
        </tr>
    );
}

export default Entry;
