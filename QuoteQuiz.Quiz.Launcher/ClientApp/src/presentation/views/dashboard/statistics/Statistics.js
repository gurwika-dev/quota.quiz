import React from "react";
import { connect } from "react-redux";
import { createSelector } from "reselect";
import { NavLink } from "react-router-dom";

import { store } from "../../../../persistence/story";
import * as mutations from "../../../../domain/mutations";

import DataTable from "../../../components/data-table/DataTable";
import PortletContainer from "../../../components/portlet-container/PortletContainer";

import Entry from "./entry/Entry";
import "./Statistics.scss";

class Statistics extends React.PureComponent {
    constructor() {
        super();
        this.state = { page: 1, limit: 10 };
    }

    componentDidMount() {
        this.handleChange(this.state);
    }

    handleChange = filter => {
        this.setState(filter);
        store.dispatch(mutations.requestStatistics(filter));
    };

    render() {
        const { totalCount, entries, percentageOfBinaryAnswers, percentageOfCorrectAnswers } = this.props;

        return (
            <div className="statistics">
                <div className="mrk-container-fluid">
                    <div className="mrk-row">
                        <div className="mrk-sm-4">
                            <div className="statistics__view statistics__view--blue">
                                <div className="statistics__view-wrapper">
                                    <div>
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                                <path d="M0 0h24v24H0z" fill="none" />
                                                <path d="M12 2c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm9 7h-6v13h-2v-6h-2v6H9V9H3V7h18v2z" />
                                            </svg>
                                        </span>
                                    </div>
                                    <p>Total</p>
                                    <h3>{totalCount}</h3>
                                </div>
                            </div>
                        </div>
                        <div className="mrk-sm-4">
                            <div className="statistics__view statistics__view--green">
                                <div className="statistics__view-wrapper">
                                    <div>
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                                <path d="M0 0h24v24H0V0z" fill="none" />
                                                <path d="M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z" />
                                            </svg>
                                        </span>
                                    </div>
                                    <p>Binary Answers</p>
                                    <h3>
                                        {percentageOfBinaryAnswers} %<small></small>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="mrk-sm-4">
                            <div className="statistics__view statistics__view--orange">
                                <div className="statistics__view-wrapper">
                                    <div>
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                                                <path d="M0 0h24v24H0z" fill="none" />
                                                <path d="M12 2c-4.42 0-8 .5-8 4v9.5C4 17.43 5.57 19 7.5 19L6 20.5v.5h12v-.5L16.5 19c1.93 0 3.5-1.57 3.5-3.5V6c0-3.5-3.58-4-8-4zM7.5 17c-.83 0-1.5-.67-1.5-1.5S6.67 14 7.5 14s1.5.67 1.5 1.5S8.33 17 7.5 17zm3.5-6H6V6h5v5zm5.5 6c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm1.5-6h-5V6h5v5z" />
                                            </svg>
                                        </span>
                                    </div>
                                    <p>Correct Answers</p>
                                    <h3>{percentageOfCorrectAnswers} %</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <PortletContainer
                        header={
                            <NavLink to="/quiz">
                                <div className="portlet-container__action">
                                    <span>
                                        <svg height="24" viewBox="0 0 24 24" width="24">
                                            <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" />
                                            <path d="M0 0h24v24H0z" fill="none" />
                                        </svg>
                                        Start
                                    </span>
                                </div>
                            </NavLink>
                        }
                    >
                        <DataTable
                            headers={["Answer", "Status"]}
                            filter={this.state}
                            data={{ totalCount, entries }}
                            component={Entry}
                            onChange={filter => {
                                this.handleChange(filter);
                            }}
                        />
                    </PortletContainer>
                </div>
            </div>
        );
    }
}

const getState = createSelector(
    state => state.statistics,
    ({ totalCount, entries, percentageOfBinaryAnswers, percentageOfCorrectAnswers }) => ({
        totalCount,
        entries,
        percentageOfBinaryAnswers,
        percentageOfCorrectAnswers
    })
);

const mapStateToProps = state => {
    return getState(state);
};

export default connect(mapStateToProps)(Statistics);
