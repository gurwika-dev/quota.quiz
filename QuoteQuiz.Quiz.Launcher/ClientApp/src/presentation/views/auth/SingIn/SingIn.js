import React from "react";
import * as yup from "yup";
import { connect } from "react-redux";
import { withFormik, Form } from "formik";
import { createSelector } from "reselect";

import * as mutations from "../../../../domain/mutations";

import Submit from "../../../components/submit/Submit";
import TextBox from "../../../components/text-box/TextBox";

import "./SingIn.scss";

class SignIn extends React.PureComponent {
    render() {
        const { errors, touched } = this.props;

        return (
            <div className="sign-in">
                <div className="sign-in__container">
                    <div className="sign-in__wrapper">
                        <div className="mrk-container">
                            <div className="mrk-row">
                                <div className="mrk-xs-12 mrk-sm-6 mrk-sm-space-left-3 mrk-md-4 mrk-md-space-left-4">
                                    <div className="sign-in__form  font-normal">
                                        <Form>
                                            <div className="sign-in__form-header">
                                                <h4>Sign in</h4>
                                            </div>
                                            <div className="sign-in__form-body">
                                                <div className="sign-in__form-field">
                                                    <TextBox
                                                        name="userName"
                                                        type="text"
                                                        placeholder="User name"
                                                        error={errors.userName}
                                                        touched={touched.userName}
                                                    />
                                                </div>
                                                <div className="sign-in__form-field">
                                                    <TextBox
                                                        name="password"
                                                        type="password"
                                                        placeholder="Password"
                                                        error={errors.password}
                                                        touched={touched.password}
                                                    />
                                                </div>
                                            </div>
                                            <div className="sign-in__form-submit">
                                                <Submit title="Continue" />
                                            </div>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const SignInFormik = withFormik({
    validationSchema: yup.object().shape({
        userName: yup
            .string()
            .email()
            .required(),
        password: yup
            .string()
            .required()
            .max(500)
            .min(6)
    }),
    mapPropsToValues: () => {
        return {
            userName: "",
            password: ""
        };
    },
    handleSubmit: (values, { props }) => {
        props.signIn(values);
    }
})(SignIn);

const getState = createSelector(
    state => state.identity,
    () => ({})
);

const mapStateToProps = state => {
    return getState(state);
};

const mapDispatchToProps = dispatch => {
    return {
        signIn({ userName, password }) {
            dispatch(mutations.requestSignIn(userName, password));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInFormik);
