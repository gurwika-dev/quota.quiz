import React from "react";
import * as yup from "yup";
import { connect } from "react-redux";
import { withFormik, Form } from "formik";
import { createSelector } from "reselect";

import * as mutations from "../../../../domain/mutations";

import Submit from "../../../components/submit/Submit";
import TextBox from "../../../components/text-box/TextBox";

import "./SignUp.scss";

class SignIn extends React.PureComponent {
    render() {
        const { errors, touched } = this.props;

        return (
            <div className="sign-up">
                <div className="sign-up__container">
                    <div className="sign-up__wrapper">
                        <div className="mrk-container">
                            <div className="mrk-row">
                                <div className="mrk-xs-12 mrk-sm-6 mrk-sm-space-left-3 mrk-md-4 mrk-md-space-left-4">
                                    <div className="sign-up__form  font-normal">
                                        <Form>
                                            <div className="sign-up__form-header">
                                                <h4>Sign up</h4>
                                            </div>
                                            <div className="sign-up__form-body">
                                                <div className="sign-up__form-field">
                                                    <TextBox
                                                        name="firstName"
                                                        type="text"
                                                        placeholder="First name"
                                                        error={errors.userName}
                                                        touched={touched.userName}
                                                    />
                                                </div>
                                                <div className="sign-up__form-field">
                                                    <TextBox
                                                        name="lastName"
                                                        type="text"
                                                        placeholder="Last name"
                                                        error={errors.userName}
                                                        touched={touched.userName}
                                                    />
                                                </div>
                                                <div className="sign-up__form-field">
                                                    <TextBox
                                                        name="userName"
                                                        type="text"
                                                        placeholder="User name"
                                                        error={errors.userName}
                                                        touched={touched.userName}
                                                    />
                                                </div>
                                                <div className="sign-up__form-field">
                                                    <TextBox
                                                        name="password"
                                                        type="password"
                                                        placeholder="Password"
                                                        error={errors.password}
                                                        touched={touched.password}
                                                    />
                                                </div>
                                            </div>
                                            <div className="sign-up__form-submit">
                                                <Submit title="Continue" />
                                            </div>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const SignInFormik = withFormik({
    validationSchema: yup.object().shape({
        firstName: yup.string().required(),
        lastName: yup.string().required(),
        userName: yup
            .string()
            .email()
            .required(),
        password: yup
            .string()
            .required()
            .max(500)
            .min(6)
    }),
    mapPropsToValues: () => {
        return {
            firstName: "",
            lastName: "",
            userName: "",
            password: ""
        };
    },
    handleSubmit: (values, { props }) => {
        props.signUp(values);
    }
})(SignIn);

const getState = createSelector(
    state => state.identity,
    () => ({})
);

const mapStateToProps = state => {
    return getState(state);
};

const mapDispatchToProps = dispatch => {
    return {
        signUp({ userName, password }) {
            dispatch(mutations.requestSignUp(userName, password));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignInFormik);
