import React from "react";
import { Route, Switch } from "react-router-dom";

import SingIn from "./SingIn/SingIn";
import SignUp from "./SignUp/SignUp";

function Auth() {
    return (
        <div className="app">
            <div className="app__wrapper">
                <div className="app__content">
                    <Switch>
                        <Route path="/auth/sign-in" component={SingIn} />
                        <Route path="/auth/sign-up" component={SignUp} />
                    </Switch>
                </div>
            </div>
        </div>
    );
}

export default Auth;
