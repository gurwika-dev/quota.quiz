export const QuizMode = {
    Binary: "0",
    Multiple: "1",
    "0": "Binary",
    "1": "Multiple"
};
