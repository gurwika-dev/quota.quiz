export const PaginationType = {
	Dots: 0,
	Page: 1,
	Active: 2,
	Prev: 3,
	PrevBlacked: 4,
	Next: 5,
	NextBlocked: 6
};
