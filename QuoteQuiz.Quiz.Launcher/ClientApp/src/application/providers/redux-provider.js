import React from 'react';
import { Provider } from 'react-redux';

import { store } from '../../persistence/story/index';

export const ReduxProvider = ({ children }) => {
	return <Provider store={store}>{children}</Provider>;
};
