import React from "react";
import { connect } from "react-redux";
import { Route, Redirect, withRouter } from "react-router-dom";
import { createSelector } from "reselect";

import Loading from "../../presentation/components/loading/Loading";

class PrivateRoute extends React.PureComponent {
    render() {
        const { isAuthenticated, isLoading, component: Component, path, location, ...rest } = this.props;

        if (isLoading) {
            return <Loading />;
        }

        const render = props => (isAuthenticated === true ? <Component {...props} /> : <Redirect to="/auth/sign-in" />);

        return <Route path={path} render={render} {...rest} />;
    }
}

const getState = createSelector(
    state => state.identity,
    ({ isAuthenticated, isLoading }) => ({
        isAuthenticated,
        isLoading
    })
);

const mapStateToProps = state => {
    return getState(state);
};

export default connect(mapStateToProps)(withRouter(PrivateRoute));
