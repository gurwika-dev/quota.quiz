import axios from "axios";
import authenticationService from "../../application/services/authentication.service";

class HttpService {
    defaultConfig;
    options = {};

    init(initOptions) {
        this.options = { ...this.options, ...initOptions };

        axios.interceptors.response.use(
            response => {
                return response;
            },
            error => {
                if (error.response.status === 401) {
                    authenticationService.signOff();
                }

                throw error;
            }
        );
    }

    getBaseUrl() {
        return this.options.apiDomain.replace(/\/$/, "");
    }

    setAccessToken(accessToken) {
        this.defaultConfig = {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        };
    }

    get(url, options = {}) {
        return axios.get(this.getBaseUrl() + "/" + url, { ...this.defaultConfig, ...options });
    }

    post(url, data, options = {}) {
        return axios.post(this.getBaseUrl() + "/" + url, data, { ...this.defaultConfig, ...options });
    }

    put(url, data, options = {}) {
        return axios.put(this.getBaseUrl() + "/" + url, data, { ...this.defaultConfig, ...options });
    }

    delete(url, options = {}) {
        return axios.delete(this.getBaseUrl() + "/" + url, { ...this.defaultConfig, ...options });
    }
}

export default new HttpService();
