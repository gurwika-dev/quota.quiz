import httpService from "../../application/services/http-service";
import cookieService from "../../application/services/cookie.service";
import axios from "axios";
import querystring from "querystring";

import { store } from "../../persistence/story/index";
import * as mutations from "../../domain/mutations";

import { QuizMode } from "../../application/types/quize-mode";

class AuthenticationService {
    options = {};
    httpClient;
    authToken;

    isAuthenticated() {
        const isAuthenticated = !!cookieService.get("isAuthenticated");

        if (isAuthenticated) {
            const accessToken = cookieService.get("accessToken");
            const mode = cookieService.get("mode");

            this.setIdentity({ isAuthenticated, accessToken, mode });
        }

        return isAuthenticated;
    }

    setMode({ mode }) {
        cookieService.set("mode", mode);
    }

    setIdentity({ isAuthenticated, accessToken, mode }) {
        httpService.setAccessToken(accessToken);
        store.dispatch(mutations.initIdentity({ isAuthenticated, accessToken, isLoading: false, mode: mode || QuizMode.Binary }));

        this.setCookie({ isAuthenticated, accessToken });
    }

    setCookie({ isAuthenticated, accessToken }) {
        cookieService.set("accessToken", accessToken);
        cookieService.set("isAuthenticated", isAuthenticated);
    }

    destroyCookie() {
        cookieService.delete("mode");
        cookieService.delete("accessToken");
        cookieService.delete("isAuthenticated");

        store.dispatch(mutations.initIdentity({ isAuthenticated: false, accessToken: null, isLoading: false }));
    }

    init(initOptions) {
        this.options = { ...this.options, ...initOptions };

        if (!this.isAuthenticated()) {
            store.dispatch(mutations.initIdentity({ isLoading: false }));
        }

        this.authToken = btoa(`${this.options.clientId}:${this.options.clientSecret}`);
        this.httpClient = axios.create({
            baseURL: `${this.options.identityDomain}`,
            timeout: 1000,
            headers: {
                Authorization: `Basic ${this.authToken}`,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        });
    }

    singIn(username, password) {
        const requestBody = {
            username,
            password,
            grant_type: "password",
            scope: "QuoteQuiz.Identity.LauncherAPI QuoteQuiz.Quiz.LauncherAPI"
        };

        return this.httpClient.post("/connect/token", querystring.stringify(requestBody));
    }

    signOff() {
        this.destroyCookie();
    }
}

export default new AuthenticationService();
