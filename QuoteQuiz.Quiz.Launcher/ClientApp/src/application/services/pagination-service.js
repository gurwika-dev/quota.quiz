import { PaginationType } from '../types/pagination-type';

class PaginationService {
	getPagination(page, totalItems, limit) {
		const pagination = [];
		const adjacents = 1;
		let counter;

		/* Setup page vars for display. */
		page = page || 0;

		const prev = page - 1;
		const next = page + 1;
		const lastpage = Math.ceil(totalItems / limit);
		const lpm1 = lastpage - 1;

		if (lastpage < 2) {
			return pagination;
		}

		if (page > 1) {
			pagination.push({ type: PaginationType.Prev, index: prev });
		}

		/* pages */
		if (lastpage < 7 + adjacents * 2) {
			for (counter = 1; counter <= lastpage; counter++) {
				if (counter === page) {
					pagination.push({ type: PaginationType.Active, index: counter });
				} else {
					pagination.push({ type: PaginationType.Page, index: counter });
				}
			}
		} else {
			/*close to beginning; only hide later pages*/
			if (page <= 1 + adjacents * 2) {
				for (counter = 1; counter < 4 + adjacents * 2; counter++) {
					if (counter === page) {
						pagination.push({ type: PaginationType.Active, index: counter });
					} else {
						pagination.push({ type: PaginationType.Page, index: counter });
					}
				}

				pagination.push({ type: PaginationType.Dots });
				pagination.push({ type: PaginationType.Page, index: lpm1 });
				pagination.push({ type: PaginationType.Page, index: lastpage });
			} else if (lastpage - adjacents * 2 > page && page > adjacents * 2) {
				/*in middle; hide some front and some back*/
				pagination.push({ type: PaginationType.Page, index: 1 });
				// pagination.push({ type: PaginationType.Page, index: 2 });
				pagination.push({ type: PaginationType.Dots });

				for (counter = page - adjacents; counter <= page + adjacents; counter++) {
					if (counter === page) {
						pagination.push({ type: PaginationType.Active, index: counter });
					} else {
						pagination.push({ type: PaginationType.Page, index: counter });
					}
				}

				pagination.push({ type: PaginationType.Dots });
				// pagination.push({ type: PaginationType.Page, index: lpm1 });
				pagination.push({ type: PaginationType.Page, index: lastpage });
			} else {
				/*close to end; only hide early pages*/
				pagination.push({ type: PaginationType.Page, index: 1 });
				pagination.push({ type: PaginationType.Page, index: 2 });
				pagination.push({ type: PaginationType.Dots });

				for (counter = lastpage - (2 + adjacents * 2); counter <= lastpage; counter++) {
					if (counter === page) {
						pagination.push({ type: PaginationType.Active, index: counter });
					} else {
						pagination.push({ type: PaginationType.Page, index: counter });
					}
				}
			}
		}

		if (page < counter - 1) {
			pagination.push({ type: PaginationType.Next, index: next });
		}

		return pagination;
	}
}

export default new PaginationService();
