import { complement } from 'ramda';
import { isNilOrEmpty } from './is-nil-or-empty';

export const isNotNilOrEmpty = complement(isNilOrEmpty);
