﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors;

namespace QuoteQuiz.Quiz.Launcher.Controllers
{
    [Authorize]
    [Route("api/authors")]
    public class AuthorsController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<FetchAuthorsVM>> Fetch([FromQuery] FetchAuthorsModel model)
        {
            return Ok(await Mediator.Send(new FetchAuthorsQuery { Model = model }));
        }
    }
}
