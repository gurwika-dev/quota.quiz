﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuoteQuiz.Quiz.Application.Quotes.Commands.CreateQuote;
using QuoteQuiz.Quiz.Application.Quotes.Commands.DeleteQuote;
using QuoteQuiz.Quiz.Application.Quotes.Commands.UpdateQuote;
using QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes;
using QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote;
using QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer;

namespace QuoteQuiz.Quiz.Launcher.Controllers
{
    [Route("api/quotes")]
    public class QuoteController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<FetchQuotesVM>> Fetch([FromQuery] FetchQuotesModel model)
        {
            return Ok(await Mediator.Send(new FetchQuotesQuery { Model = model }));
        }

        [HttpGet("{quoteId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetQuoteVM>> Get(Guid quoteId)
        {
            return Ok(await Mediator.Send(new GetQuoteQuery { QuoteId = quoteId }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody]CreateQuoteModel model)
        {
            await Mediator.Send(new CreateQuoteCommand { Model = model });

            return NoContent();
        }

        [HttpPut("{quoteId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Update(Guid quoteId, [FromBody]UpdateQuoteModel model)
        {
            await Mediator.Send(new UpdateQuoteCommand { QuoteId = quoteId, Model = model });

            return NoContent();
        }

        [HttpDelete("{quoteId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid quoteId)
        {
            await Mediator.Send(new DeleteQuoteCommand { QuoteId = quoteId });

            return NoContent();
        }

        [HttpGet("{quoteId}/answer")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetQuoteAnswerVM>> GetAnswer(Guid quoteId)
        {
            return Ok(await Mediator.Send(new GetQuoteAnswerQuery { QuoteId = quoteId }));
        }
    }
}
