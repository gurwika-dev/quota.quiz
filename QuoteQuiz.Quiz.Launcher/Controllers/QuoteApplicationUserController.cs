﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuoteQuiz.Quiz.Application.ApplicationUser.Commands.CreateQuoteApplicationUser;
using QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser;
using QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics;

namespace QuoteQuiz.Quiz.Launcher.Controllers
{
    [Route("api/quote-application-users")]
    public class QuoteApplicationUserController : BaseController
    {
        [HttpGet("statistics")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetApplicationUseStatisticsVM>> Statistics([FromQuery] GetApplicationUseStatisticsModel model)
        {
            return Ok(await Mediator.Send(new GetApplicationUseStatisticsQuery { Model = model }));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<FetchQuoteApplicationUserVM>> Fetch([FromQuery] FetchQuoteApplicationUserModel model)
        {
            return Ok(await Mediator.Send(new FetchQuoteApplicationUserQuery { Model = model }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Anwer([FromBody]CreateQuoteApplicationUserModel model)
        {
            await Mediator.Send(new CreateQuoteApplicationUserCommand { Model = model });

            return NoContent();
        }
    }
}