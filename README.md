# Quote quiz

This application includes thee logical component:

-   `QuoteQuiz.Common`
-   `QuoteQuiz.Identity`
-   `QuoteQuiz.Quiz`

and one [React](https://reactjs.org/) Client Application which runs using Asp.net Core to Build Single-page Applications

## Configuration

First, you need to configure `appsettings.Development.json` or `appsettings.Production.json`, depending on you environment variables.

Additionally, you need to configure `.env.development` or `.env.production`, depending on you environment variables.

## Database

You do not need any **_Backup of the Database_**, migrations runs automatically and seed-data runs for testing purposes

## Usage

Then you should run **_QuoteQuiz.Identity.Launcher_** and **_QuoteQuiz.Quiz.Launcher_** projects to get fully functional application.

## Server side

once, you run the application can access the swagger using following links:

-   [QuoteQuiz.Identity](https://localhost:44381/api/index.html)
-   [QuoteQuiz.Quiz](https://localhost:44343/api/index.html)

## Client side

You can not access application without `authorization`. Once you run **_QuoteQuiz.Identity.Launcher_** project, seed-data inserts test user to the database with the following credentials, you can use then to login:

`demouser@demo.com`

`Pass@word1`

To change the game mode, please see the `settings` icon on the right top side of the screen.

## License

[MIT](https://choosealicense.com/licenses/mit/)
