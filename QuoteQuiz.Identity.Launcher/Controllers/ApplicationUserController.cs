﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuoteQuiz.Identity.Application.ApplicationUsers.Commands.CreateApplicationUser;
using QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DeleteApplicationUser;
using QuoteQuiz.Identity.Application.ApplicationUsers.Commands.DisableApplicationUser;
using QuoteQuiz.Identity.Application.ApplicationUsers.Commands.EnableApplicationUser;
using QuoteQuiz.Identity.Application.ApplicationUsers.Commands.UpdateApplicationUser;
using QuoteQuiz.Identity.Application.ApplicationUsers.Queries.FetchApplicationUsers;
using QuoteQuiz.Identity.Application.ApplicationUsers.Queries.GetApplicationUser;

namespace QuoteQuiz.Identity.Launcher.Controllers
{
    [Route("api/application-user")]
    public class ApplicationUserController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<FetchApplicationUsersVM>> GetAll([FromQuery] FetchApplicationUsersModel model)
        {
            return Ok(await Mediator.Send(new FetchApplicationUsersQuery { Model = model }));
        }

        [HttpGet("{applicationUserId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetApplicationUserVM>> Get(Guid applicationUserId)
        {
            return Ok(await Mediator.Send(new GetApplicationUserQuery { ApplicationUserId = applicationUserId }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody]CreateApplicationUserModel model)
        {
            await Mediator.Send(new CreateApplicationUserCommand { Model = model });

            return NoContent();
        }

        [HttpPut("{applicationUserId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Update(Guid applicationUserId, [FromBody]UpdateApplicationUserModel model)
        {
            await Mediator.Send(new UpdateApplicationUserCommand { ApplicationUserId = applicationUserId, Model = model });

            return NoContent();
        }

        [HttpPut("{applicationUserId}/enable")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Endable(Guid applicationUserId)
        {
            await Mediator.Send(new EnableApplicationUserCommand { ApplicationUserId = applicationUserId });

            return NoContent();
        }

        [HttpPut("{applicationUserId}/disable")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Disable(Guid applicationUserId)
        {
            await Mediator.Send(new DisableApplicationUserCommand { ApplicationUserId = applicationUserId });

            return NoContent();
        }

        [HttpDelete("{applicationUserId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid applicationUserId)
        {
            await Mediator.Send(new DeleteApplicationUserCommand { ApplicationUserId = applicationUserId });

            return NoContent();
        }
    }
}