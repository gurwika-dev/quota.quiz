﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuoteQuiz.Common.Domain.Entities.Quiz;

namespace QuoteQuiz.Common.Persistence.Configuration.Quiz
{
    public class QuoteConfiguration : IEntityTypeConfiguration<Quote>
    {
        public void Configure(EntityTypeBuilder<Quote> builder)
        {
            builder.Ignore(c => c.PendingEvents);

            builder.Property(e => e.Text)
                .IsRequired()
                .HasMaxLength(2500);

            builder.HasOne(e => e.Author)
                .WithMany(e => e.Quotes)
                .HasForeignKey(e => e.AuthorId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}