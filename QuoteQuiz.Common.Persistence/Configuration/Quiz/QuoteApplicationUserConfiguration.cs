﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuoteQuiz.Common.Domain.Entities.Quiz;

namespace QuoteQuiz.Common.Persistence.Configuration.Quiz
{
    public class QuoteApplicationUserConfiguration : IEntityTypeConfiguration<QuoteApplicationUser>
    {
        public void Configure(EntityTypeBuilder<QuoteApplicationUser> builder)
        {
            builder.Ignore(c => c.PendingEvents);

            builder.Property(e => e.Answer)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne(e => e.ApplicationUser)
                .WithMany(e => e.QuoteQuotes)
                .HasForeignKey(e => e.ApplicationUserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(e => e.Quote)
                .WithMany(e => e.QuoteQuotes)
                .HasForeignKey(e => e.QuoteId);
        }
    }
}
