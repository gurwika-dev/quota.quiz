﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuoteQuiz.Common.Domain.Entities.Quiz;

namespace QuoteQuiz.Common.Persistence.Configuration.Quiz
{
    public class AuthorConfiguration : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.Property(e => e.DisplayName)
                .IsRequired()
                .HasMaxLength(200);
        }
    }
}