﻿using QuoteQuiz.Common.Persistence.Context;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Application;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using IdentityServer4.Services;
using QuoteQuiz.Common.Infrastructure.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using IdentityModel;

namespace QuoteQuiz.Common.Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommonPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies()
                .UseNpgsql(
                    configuration.GetConnectionString("ApplicationDbContext"), 
                    x => x.MigrationsHistoryTable(HistoryRepository.DefaultTableName)
                )
            );

            services.AddDefaultIdentity<ApplicationUser>(options =>
                {
                    // Password settings
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 8;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireLowercase = false;
                })
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddScoped<IApplicationDbContext>(x => x.GetService<ApplicationDbContext>());

            return services;
        }

        public static IServiceCollection AddCommonAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication()
                .AddIdentityServerJwt();


            services.AddTransient<IProfileService, ProfileService>();

            return services;
        }

        public static IIdentityServerBuilder AddSigningCredential(this IIdentityServerBuilder builder, IConfigurationSection options, IWebHostEnvironment environment, string projectName)
        {
            var keyFileName = options.GetValue<string>("KeyFilePath");
            var keyFilePassword = options.GetValue<string>("KeyFilePassword");
            var contentRootPath = environment.ContentRootPath.Substring(0, environment.ContentRootPath.LastIndexOf(projectName, StringComparison.Ordinal));

            var keyFilePath = contentRootPath + "\\" + keyFileName;

            builder.AddSigningCredential(new X509Certificate2(keyFilePath, keyFilePassword));

            return builder;
        }
    }
}
