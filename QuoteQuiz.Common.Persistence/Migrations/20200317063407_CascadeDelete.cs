﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuoteQuiz.Common.Persistence.Migrations
{
    public partial class CascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuoteApplicationUser_AspNetUsers_ApplicationUserId",
                table: "QuoteApplicationUser");

            migrationBuilder.AddForeignKey(
                name: "FK_QuoteApplicationUser_AspNetUsers_ApplicationUserId",
                table: "QuoteApplicationUser",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuoteApplicationUser_AspNetUsers_ApplicationUserId",
                table: "QuoteApplicationUser");

            migrationBuilder.AddForeignKey(
                name: "FK_QuoteApplicationUser_AspNetUsers_ApplicationUserId",
                table: "QuoteApplicationUser",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
