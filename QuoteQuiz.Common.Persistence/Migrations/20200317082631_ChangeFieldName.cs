﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuoteQuiz.Common.Persistence.Migrations
{
    public partial class ChangeFieldName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Quote");

            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Quote",
                maxLength: 2500,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Text",
                table: "Quote");

            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Quote",
                type: "character varying(2500)",
                maxLength: 2500,
                nullable: false,
                defaultValue: "");
        }
    }
}
