﻿using Microsoft.AspNetCore.Http;
using Moq;
using System.Net;
using QuoteQuiz.Common.Infrastructure.Services;
using Xunit;
using System.Security.Claims;

namespace QuoteQuiz.Common.Infrastructure.Test.Utils
{
    public class CurrentUserServiceTests
    {
        
        private readonly string _userId;
        private readonly Claim _claim;
        private readonly IPAddress _ipAddress;
        private readonly CurrentUserService _currentUserService;

        private readonly Mock<IHttpContextAccessor> _httpContextAccessorMock;
        private readonly Mock<HttpContext> _httpContextMock;
        private readonly Mock<ConnectionInfo> _connectionMock;
        private readonly Mock<ClaimsPrincipal> _user;


        public CurrentUserServiceTests()
        {
            _userId = "00000000-0000-0000-0000-000000000000";
            _claim = new Claim(ClaimTypes.NameIdentifier, _userId);

            _ipAddress = IPAddress.Parse("127.0.0.1");
            _connectionMock = new Mock<ConnectionInfo>();
            _connectionMock.Setup(m => m.RemoteIpAddress).Returns(_ipAddress);

            _user = new Mock<ClaimsPrincipal>();
            _user.Setup(x => x.FindFirst(It.IsAny<string>())).Returns(_claim);

            _httpContextMock = new Mock<HttpContext>();
            _httpContextMock.Setup(m => m.Connection).Returns(_connectionMock.Object);
            _httpContextMock.Setup(m => m.User).Returns(_user.Object);

            _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            _httpContextAccessorMock.Setup(m => m.HttpContext).Returns(_httpContextMock.Object);

            _currentUserService = new CurrentUserService(_httpContextAccessorMock.Object);
        }

        [Fact]
        public void ShouldHaveCorrectIpAddress()
        {
            Assert.Equal(_ipAddress, _currentUserService.Ip);
        }

        [Fact]
        public void ShouldBeAuthenticateds()
        {
            Assert.True(_currentUserService.IsAuthenticated);
            Assert.Equal(_userId, _currentUserService.UserId);
        }
    }
}
