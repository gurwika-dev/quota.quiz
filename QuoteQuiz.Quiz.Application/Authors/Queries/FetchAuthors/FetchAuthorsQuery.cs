﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;

namespace QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors
{
    public class FetchAuthorsQuery : IQuery<FetchAuthorsVM> 
    { 
        public FetchAuthorsModel Model { get; set; }
    }
}
