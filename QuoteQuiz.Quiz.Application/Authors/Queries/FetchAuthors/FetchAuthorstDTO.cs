﻿using AutoMapper;
using System;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;

namespace QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors
{
    public class FetchAuthorstDTO : IMapFrom<Author>
    {
        public Guid Id { get; set; }
        public string DisplayName { get; protected set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Author, FetchAuthorstDTO>();
        }
    }
}
