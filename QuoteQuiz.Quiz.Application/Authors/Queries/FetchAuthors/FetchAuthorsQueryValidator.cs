﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors
{
    public class FetchAuthorsQueryValidator : AbstractValidator<FetchAuthorsQuery>
    {
        public FetchAuthorsQueryValidator()
        {
            RuleFor(x => x.Model.KeyWord).MaximumLength(10);
        }
    }
}
