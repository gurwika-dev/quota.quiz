﻿using System.Collections.Generic;

namespace QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors
{
    public class FetchAuthorsVM
    {
        public IList<FetchAuthorstDTO> Entries { get; set; }
        public int TotalCount { get; set; }
    }
}
