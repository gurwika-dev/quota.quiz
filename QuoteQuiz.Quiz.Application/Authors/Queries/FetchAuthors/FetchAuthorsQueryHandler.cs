﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;

namespace QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors
{
    public class FetchAuthorsQueryHandler : IQueryHandler<FetchAuthorsQuery, FetchAuthorsVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public FetchAuthorsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<FetchAuthorsVM> Handle(FetchAuthorsQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<Author>().Where(x => true);

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                entriesQuery = entriesQuery.Where(e => e.DisplayName.Contains(request.Model.KeyWord));
            }

            var totalCount = entriesQuery.Count();
            var entries = await entriesQuery
                                    .ProjectTo<FetchAuthorstDTO>(_mapper.ConfigurationProvider)
                                    .ToListAsync(cancellationToken);

            return new FetchAuthorsVM
            {
                Entries = entries,
                TotalCount = totalCount
            };
        }
    }
}
