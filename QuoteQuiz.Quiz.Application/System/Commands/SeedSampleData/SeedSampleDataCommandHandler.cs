﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;
using System.Threading;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Infrastructure.Extentions;
using System.Collections.Generic;
using QuoteQuiz.Common.Application.Common.Handlers;

namespace QuoteQuiz.Quiz.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommandHandler: CommandHandler, ICommandHandler<SeedSampleDataCommand>
    {
        public SeedSampleDataCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(SeedSampleDataCommand request, CancellationToken cancellationToken)
        {
            var authors = new List<Author> {
                new Author(Guid.NewGuid(), "Om Philip"),
                new Author(Guid.NewGuid(), "Faraz Dejesus"),
                new Author(Guid.NewGuid(), "Esha English"),
                new Author(Guid.NewGuid(), "Reece Handley"),
                new Author(Guid.NewGuid(), "Manahil Brown"),
                new Author(Guid.NewGuid(), "Nina Moreno"),
                new Author(Guid.NewGuid(), "Carrie - Ann Robertson"),
                new Author(Guid.NewGuid(), "Simona Alvarez"),
                new Author(Guid.NewGuid(), "Perry Solis" ),
                new Author(Guid.NewGuid(), "Danica Compton"),
                new Author(Guid.NewGuid(), "Harriette Sadler"),
                new Author(Guid.NewGuid(),  "Mihai Ferry"),
                new Author(Guid.NewGuid(), "Nylah Nunez"),
                new Author(Guid.NewGuid(), "Bronwyn Britt"),
                new Author(Guid.NewGuid(), "Paloma Cartwright"),
                new Author(Guid.NewGuid(), "Zain Pearson" ),
                new Author(Guid.NewGuid(), "Betty Henry"),
                new Author(Guid.NewGuid(), "Sukhmani Hayden"),
                new Author(Guid.NewGuid(), "Millicent Lam"),
                new Author(Guid.NewGuid(), "Sarah - Jane Brady" ),
            };

            var quotes = new List<Quote> { 
                new Quote(Guid.NewGuid(), "Your internal thoughts become your external life.", authors[0].Id),
                new Quote(Guid.NewGuid(), "Your only duty is to your heart.", authors[1].Id),
                new Quote(Guid.NewGuid(), "It is the very use of coercion, positive or negative, that breaks or deadens the spirit, which is the source of motivation.", authors[2].Id),
                new Quote(Guid.NewGuid(), "Let time be a motivator to start living your dreams now.", authors[3].Id),
                new Quote(Guid.NewGuid(), "Money isn't success. Success is the character with which you make it.", authors[4].Id),
                new Quote(Guid.NewGuid(), "Iniquity shall be increased above that which now thou see, or that thou hast heard long ago.", authors[5].Id),
                new Quote(Guid.NewGuid(), "Knowledge if churned like milk, results in the butter of wisdom.", authors[6].Id),
                new Quote(Guid.NewGuid(), "The final magnificent spark of a firework is only the last seconds of the fall. Though it'Â€Â™s invisible to most, it'Â€Â™s the way up that creates all the impact.Ã‚Â ", authors[7].Id),
                new Quote(Guid.NewGuid(), "Conflicts are expensive.", authors[8].Id),
                new Quote(Guid.NewGuid(), "Don't let your dreams stay buried under the comfort of your current success.", authors[0].Id),
                new Quote(Guid.NewGuid(), "Work on yourself'Â€Â” Feed your mind Nourish your soul Find your passion; Water it, nurture itLet it take root and blossom Let yourself be consumedLet yourself drown in what you love", authors[10].Id),
                new Quote(Guid.NewGuid(), "Our mortality makes it impossible for our problems to last forever.", authors[11].Id),
            };

            authors.ForEach(author => {
                _context.Set<Author>().AddIfNotExists(author, x => x.DisplayName == author.DisplayName);
            });

            quotes.ForEach(quote => {
                _context.Set<Quote>().AddIfNotExists(quote, x => x.Text == quote.Text);
            });

            await SaveChanges(cancellationToken);

            return Unit.Value;
        }
    }
}
