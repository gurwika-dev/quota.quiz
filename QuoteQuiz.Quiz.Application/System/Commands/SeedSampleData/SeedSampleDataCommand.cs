﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;

namespace QuoteQuiz.Quiz.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommand : ICommand { }
}
