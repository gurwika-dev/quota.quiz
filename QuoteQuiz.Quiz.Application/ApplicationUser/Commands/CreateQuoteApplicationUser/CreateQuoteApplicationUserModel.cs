﻿using QuoteQuiz.Common.Domain.Enumarations.Application;
using System;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Commands.CreateQuoteApplicationUser
{
    public class CreateQuoteApplicationUserModel
    {
        public Guid Id { get; set; }
        public QuizeMode Mode { get; set; }
        public Guid QuoteId { get; set; }
        public Guid AuthorId { get; set; }
        public bool isAuthor { get; set; }
    }
}
