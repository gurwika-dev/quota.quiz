﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Commands.CreateQuoteApplicationUser
{
    public class CreateQuoteApplicationUserValidator : AbstractValidator<CreateQuoteApplicationUserCommand>
    {
        public CreateQuoteApplicationUserValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Id).NotNull();
            RuleFor(x => x.Model.Mode).NotNull();
            RuleFor(x => x.Model.AuthorId).NotNull();
            RuleFor(x => x.Model.isAuthor).NotNull();
        }
    }
}
