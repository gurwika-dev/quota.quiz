﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Commands.CreateQuoteApplicationUser
{
    public class CreateQuoteApplicationUserCommandHandler : CommandHandler, ICommandHandler<CreateQuoteApplicationUserCommand>
    {
        private readonly ICurrentUserService _currentUserService;

        public CreateQuoteApplicationUserCommandHandler(IApplicationDbContext context, IEventBus eventBus, ICurrentUserService currentUserService) : base(context, eventBus)
        {
            _currentUserService = currentUserService;
        }

        public async Task<Unit> Handle(CreateQuoteApplicationUserCommand request, CancellationToken cancellationToken)
        {
            var quote = _context.Set<Quote>().SingleOrDefault(e => e.Id == request.Model.QuoteId);

            if (quote == null)
            {
                throw new DomainException("Quote does not exists");
            }

            var userAnswerAuthor = _context.Set<Author>().SingleOrDefault(e => e.Id == request.Model.AuthorId);

            if (userAnswerAuthor == null)
            {
                throw new DomainException("Author does not exists");
            }

            var quoteApplicationUser = new QuoteApplicationUser(
                request.Model.Id,
                request.Model.Mode,
                _currentUserService.UserId,
                quote,
                userAnswerAuthor,
                request.Model.isAuthor
            );

            _context.Add(quoteApplicationUser);

            await SaveAndPublish(quote, cancellationToken); ;

            return Unit.Value;
        }
    }
}
