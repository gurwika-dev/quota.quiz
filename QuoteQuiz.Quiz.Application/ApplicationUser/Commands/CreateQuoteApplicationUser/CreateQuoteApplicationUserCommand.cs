﻿
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Commands.CreateQuoteApplicationUser
{
    public class CreateQuoteApplicationUserCommand : ICommand
    {
        public CreateQuoteApplicationUserModel Model { get; set; }
    }
}
