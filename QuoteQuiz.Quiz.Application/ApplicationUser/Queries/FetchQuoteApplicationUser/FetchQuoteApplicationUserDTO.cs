﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors;
using System;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{
    public class FetchQuoteApplicationUserDTO : IMapFrom<Quote>
    {
        public string Text { get; set; }
        public Guid QuoteId { get; set; }
        public FetchAuthorstDTO[] Answers { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Quote, FetchQuoteApplicationUserDTO>()
                .ForMember(d => d.QuoteId, opt => opt.MapFrom(s => s.Id));
        }
    }
}
