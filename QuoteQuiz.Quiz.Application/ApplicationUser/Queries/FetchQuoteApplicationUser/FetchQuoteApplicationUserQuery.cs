﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{
    public class FetchQuoteApplicationUserQuery : IQuery<FetchQuoteApplicationUserVM>
    {
        public FetchQuoteApplicationUserModel Model { get; set; }
    }
}
