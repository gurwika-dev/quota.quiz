﻿using QuoteQuiz.Common.Domain.Enumarations.Application;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{
    public class FetchQuoteApplicationUserModel
    {
        public QuizeMode Mode { get; set; }
    }
}
