﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using QuoteQuiz.Common.Domain.Exceptions;
using QuoteQuiz.Common.Infrastructure.Extentions;
using QuoteQuiz.Quiz.Application.Cities.Queries.FetchAuthors;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{
    public class FetchQuoteApplicationUserQueryHandler : QueryHandler, IQueryHandler<FetchQuoteApplicationUserQuery, FetchQuoteApplicationUserVM>
    {
        private readonly ICurrentUserService _currentUserService;

        public FetchQuoteApplicationUserQueryHandler(IApplicationDbContext context, IMapper mapper, ICurrentUserService currentUserService) : base(context, mapper)
        {
            _currentUserService = currentUserService;
        }

        public async Task<FetchQuoteApplicationUserVM> Handle(FetchQuoteApplicationUserQuery request, CancellationToken cancellationToken)
        {
            var answeredQuotes = await _context.Set<QuoteApplicationUser>()
                                        .Where(x => x.ApplicationUserId == _currentUserService.UserId)
                                        .Select(x => x.QuoteId)
                                        .ToListAsync();

            var quoteQuery = _context.Set<Quote>()
                            .Where(x => !answeredQuotes.Contains(x.Id));

            if(quoteQuery.Count() == 0)
            {
                throw new DomainException("Quote was not found");
            }

            var random = new Random();
            int toSkip = random.Next(0, quoteQuery.Count() - 1);
            var quote = quoteQuery
                            .Skip(toSkip)
                            .Take(1)
                            .SingleOrDefault();

            var correctAuthor = _context.Set<Author>()
                                    .ProjectTo<FetchAuthorstDTO>(_mapper.ConfigurationProvider)
                                    .SingleOrDefault(x => x.Id == quote.AuthorId);

            var randAuthorsQuery = _context.Set<Author>()
                                    .Where(x => x.Id != quote.AuthorId);

            toSkip = random.Next(0, randAuthorsQuery.Count() - 1);
            var randAuthors = await randAuthorsQuery
                                .ProjectTo<FetchAuthorstDTO>(_mapper.ConfigurationProvider)
                                .Skip(toSkip)
                                .Take(2)
                                .ToListAsync();

            randAuthors.Add(correctAuthor);

            if (request.Model.Mode == QuizeMode.Binary)
            {
                randAuthors.RemoveAt(0);
            }

            randAuthors = randAuthors.OrderBy(_ => Guid.NewGuid()).ToList();

            if (request.Model.Mode == QuizeMode.Binary)
            {
                randAuthors = randAuthors.Take(1).ToList();
            }

            var entry = new FetchQuoteApplicationUserDTO
            {
                Answers = randAuthors.ToArray(),
                Text = quote.Text,
                QuoteId = quote.Id
            };

            return new FetchQuoteApplicationUserVM
            {
                Entry = entry
            };
        }
    }
}
