﻿namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{
    public class FetchQuoteApplicationUserVM
    {
        public FetchQuoteApplicationUserDTO Entry { get; set; }
    }
}
