﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.FetchQuoteApplicationUser
{

    public class FetchQuoteApplicationUserQueryValidator : AbstractValidator<FetchQuoteApplicationUserQuery>
    {
        public FetchQuoteApplicationUserQueryValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Mode).NotNull();
        }
    }
}
