﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsQuery : IQuery<GetApplicationUseStatisticsVM>
    {
        public GetApplicationUseStatisticsModel Model { get; set; }
    }
}
