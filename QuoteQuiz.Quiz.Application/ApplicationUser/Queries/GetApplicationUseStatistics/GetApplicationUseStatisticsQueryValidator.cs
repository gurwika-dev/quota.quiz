﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsQueryValidator : AbstractValidator<GetApplicationUseStatisticsQuery>
    {
        public GetApplicationUseStatisticsQueryValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Page).GreaterThanOrEqualTo(1).NotNull();
            RuleFor(x => x.Model.Limit).GreaterThan(1).NotNull();
            RuleFor(x => x.Model.KeyWord).MaximumLength(50);
        }
    }
}
