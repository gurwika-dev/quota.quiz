﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using System;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsDTO : IMapFrom<QuoteApplicationUser>
    {
        public Guid id { get; set; }
        public QuizeMode Mode { get; set; }
        public string Text { get; set; }
        public string Answer { get; set; }
        public bool IsCorrect { get; set; }
        public DateTime CreatedAt { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<QuoteApplicationUser, GetApplicationUseStatisticsDTO>()
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Quote.Text));
        }
    }
}
