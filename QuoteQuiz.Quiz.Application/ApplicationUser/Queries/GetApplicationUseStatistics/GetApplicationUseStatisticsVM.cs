﻿using System.Collections.Generic;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsVM
    {
        public List<GetApplicationUseStatisticsDTO> Entries { get; set; }
        public int TotalCount { get; set; }
        public int PercentageOfBinaryAnswers { get; set; }
        public int PercentageOfCorrectAnswers { get; set; }
    }
}
