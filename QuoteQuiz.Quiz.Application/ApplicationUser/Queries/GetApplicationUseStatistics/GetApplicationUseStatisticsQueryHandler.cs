﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Enumarations.Application;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsQueryHandler : QueryHandler, IQueryHandler<GetApplicationUseStatisticsQuery, GetApplicationUseStatisticsVM>
    {
        private readonly ICurrentUserService _currentUserService;

        public GetApplicationUseStatisticsQueryHandler(IApplicationDbContext context, IMapper mapper, ICurrentUserService currentUserService) : base(context, mapper)
        {
            _currentUserService = currentUserService;
        }

        public async Task<GetApplicationUseStatisticsVM> Handle(GetApplicationUseStatisticsQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<QuoteApplicationUser>().Where(x => x.ApplicationUserId == _currentUserService.UserId);

            var totalCount = entriesQuery.Count();

            var percentageOfBinaryAnswers = totalCount > 0 ? (int)(entriesQuery.Where(x => x.Mode == QuizeMode.Binary).Count() / (double)totalCount * 100.0) : 0;
            var percentageOfCorrectAnswers = totalCount > 0 ? (int)(entriesQuery.Where(x => x.IsCorrect == true).Count() / (double)totalCount * 100.0) : 0;

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                entriesQuery = entriesQuery.Where(e => e.Answer.Contains(request.Model.KeyWord));
            }

            var entries = await entriesQuery
                                    .Include(x => x.Quote)
                                    .ProjectTo<GetApplicationUseStatisticsDTO>(_mapper.ConfigurationProvider)
                                    .Skip(request.Model.Limit * (request.Model.Page - 1))
                                    .Take(request.Model.Limit)
                                    .ToListAsync(cancellationToken);

            return new GetApplicationUseStatisticsVM
            {
                Entries = entries,
                TotalCount = totalCount,
                PercentageOfBinaryAnswers = percentageOfBinaryAnswers,
                PercentageOfCorrectAnswers = percentageOfCorrectAnswers
            };
        }
    }
}
