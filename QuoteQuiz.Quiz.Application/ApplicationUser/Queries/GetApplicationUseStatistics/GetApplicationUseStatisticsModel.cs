﻿
namespace QuoteQuiz.Quiz.Application.ApplicationUser.Queries.GetApplicationUseStatistics
{
    public class GetApplicationUseStatisticsModel
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string KeyWord { get; set; }
    }
}
