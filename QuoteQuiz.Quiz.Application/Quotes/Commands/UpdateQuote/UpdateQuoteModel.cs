﻿using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.UpdateQuote
{
    public class UpdateQuoteModel
    {
        public string Text { get; set; }
        public Guid AuthorId { get; set; }
    }
}
