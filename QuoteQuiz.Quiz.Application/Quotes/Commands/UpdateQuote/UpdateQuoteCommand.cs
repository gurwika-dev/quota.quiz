﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.UpdateQuote
{
    public class UpdateQuoteCommand : ICommand
    {
        public Guid QuoteId { get; set; }
        public UpdateQuoteModel Model { get; set; }
    }
}
