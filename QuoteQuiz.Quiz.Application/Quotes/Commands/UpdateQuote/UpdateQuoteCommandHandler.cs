﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace QuoteQuiz.Quiz.Application.Quotes.Commands.UpdateQuote
{
    public class UpdateQuoteCommandHandler : CommandHandler, ICommandHandler<UpdateQuoteCommand>
    {
        public UpdateQuoteCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus)
        {
        }

        public async Task<Unit> Handle(UpdateQuoteCommand request, CancellationToken cancellationToken)
        {
            var quote = _context.Set<Quote>().SingleOrDefault(e => e.Id == request.QuoteId);

            if (quote == null)
            {
                throw new DomainException("Quote does not exists");
            }

            var author = _context.Set<Author>().SingleOrDefault(e => e.Id == request.Model.AuthorId);

            if (author == null)
            {
                throw new DomainException("Author does not exists");
            }

            quote.Update(
                request.Model.Text,
                request.Model.AuthorId
            );

            await SaveAndPublish(quote, cancellationToken); ;

            return Unit.Value;
        }
    }
}
