﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.UpdateQuote
{
    public class UpdateQuoteCommandValidator : AbstractValidator<UpdateQuoteCommand>
    {
        public UpdateQuoteCommandValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.QuoteId).NotNull();
            RuleFor(x => x.Model.AuthorId).NotNull();
            RuleFor(x => x.Model.Text).NotNull().MinimumLength(2).MaximumLength(2500);
        }
    }
}
