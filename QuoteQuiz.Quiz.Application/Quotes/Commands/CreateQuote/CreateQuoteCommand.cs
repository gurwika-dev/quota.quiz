﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.CreateQuote
{
    public class CreateQuoteCommand : ICommand
    {
        public CreateQuoteModel Model { get; set; }
    }
}
