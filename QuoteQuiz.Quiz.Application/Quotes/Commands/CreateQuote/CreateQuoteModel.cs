﻿using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.CreateQuote
{
    public class CreateQuoteModel
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public Guid AuthorId { get; set; }
    }
}
