﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.CreateQuote
{
    public class CreateQuoteCommandValidator : AbstractValidator<CreateQuoteCommand>
    {
        public CreateQuoteCommandValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Id).NotNull();
            RuleFor(x => x.Model.AuthorId).NotNull();
            RuleFor(x => x.Model.Text).NotNull().MinimumLength(2).MaximumLength(2500);
        }
    }
}
