﻿using MediatR;
using QuoteQuiz.Common.Application.Common.Abstraction;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.DeleteQuote
{
    public class DeleteQuoteCommandHandler : CommandHandler, ICommandHandler<DeleteQuoteCommand>
    {
        public DeleteQuoteCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(DeleteQuoteCommand request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Quote>().SingleOrDefault(e => e.Id == request.QuoteId);

            if (person == null)
            {
                throw new DomainException("Quote does not exists");
            }

            person.Delete();

            await SaveAndPublish(person, cancellationToken);

            return Unit.Value;
        }
    }
}
