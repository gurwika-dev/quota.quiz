﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.DeleteQuote
{
    public class DeleteQuoteCommandValidator : AbstractValidator<DeleteQuoteCommand>
    {
        public DeleteQuoteCommandValidator()
        {
            RuleFor(x => x.QuoteId).NotNull();
        }
    }
}
