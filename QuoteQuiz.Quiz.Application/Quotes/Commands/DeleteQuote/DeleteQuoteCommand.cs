﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Commands;
using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Commands.DeleteQuote
{
    public class DeleteQuoteCommand : ICommand
    {
        public Guid QuoteId { get; set; }
    }
}
