﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer
{
    public class GetQuoteAnswerDTO : IMapFrom<Quote>
    {
        public Guid Id { get; set; }
        public string AuthorName { get; set; }
        public Guid AuthorId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Quote, GetQuoteAnswerDTO>()
                .ForMember(
                    dest => dest.AuthorName,
                    opt => opt.MapFrom(src => src.Author.DisplayName)
                );
        }
    }
}
