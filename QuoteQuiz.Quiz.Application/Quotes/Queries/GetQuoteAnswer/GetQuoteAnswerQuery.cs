﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer
{
    public class GetQuoteAnswerQuery : IQuery<GetQuoteAnswerVM>
    {
        public Guid QuoteId { get; set; }
    }
}
