﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer
{
    public class GetQuoteAnswerQueryHandler : QueryHandler, IQueryHandler<GetQuoteAnswerQuery, GetQuoteAnswerVM>
    {
        public GetQuoteAnswerQueryHandler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<GetQuoteAnswerVM> Handle(GetQuoteAnswerQuery request, CancellationToken cancellationToken)
        {
            var entry = _context.Set<Quote>()
                            .Where(e => e.Id == request.QuoteId)
                                    .Include(e => e.Author)
                                    .ProjectTo<GetQuoteAnswerDTO>(_mapper.ConfigurationProvider)
                                        .FirstOrDefault();

            if (entry == null)
            {
                throw new DomainException("Quote does not exists");
            }

            return await Task.Run(() =>
            {
                return new GetQuoteAnswerVM
                {
                    Entry = entry
                };
            });
        }
    }
}
