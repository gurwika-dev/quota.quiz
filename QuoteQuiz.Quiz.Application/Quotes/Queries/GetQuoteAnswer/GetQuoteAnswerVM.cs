﻿namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer
{
    public class GetQuoteAnswerVM
    {
        public GetQuoteAnswerDTO Entry { get; set; }
    }
}
