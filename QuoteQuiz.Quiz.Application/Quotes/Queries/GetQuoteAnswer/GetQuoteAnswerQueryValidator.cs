﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuoteAnswer
{
    public class GetQuoteAnswerQueryValidator : AbstractValidator<GetQuoteAnswerQuery>
    {
        public GetQuoteAnswerQueryValidator()
        {
            RuleFor(x => x.QuoteId).NotNull();
        }
    }
}
