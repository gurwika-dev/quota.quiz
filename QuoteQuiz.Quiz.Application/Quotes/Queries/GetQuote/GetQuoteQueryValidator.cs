﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote
{
    public class GetQuoteQueryValidator : AbstractValidator<GetQuoteQuery>
    {
        public GetQuoteQueryValidator()
        {
            RuleFor(x => x.QuoteId).NotNull();
        }
    }
}
