﻿namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote
{
    public class GetQuoteVM
    {
        public GetQuoteDTO Entry { get; set; }
    }
}
