﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote
{
    public class GetQuoteQuery : IQuery<GetQuoteVM>
    {
        public Guid QuoteId { get; set; }
    }
}
