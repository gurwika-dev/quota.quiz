﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote
{
    public class GetQuoteDTO : IMapFrom<Quote>
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public Guid AuthorId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Quote, GetQuoteDTO>();
        }
    }
}
