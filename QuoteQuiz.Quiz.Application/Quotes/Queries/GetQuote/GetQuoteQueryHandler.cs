﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using QuoteQuiz.Common.Application.Common.Handlers;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Domain.Exceptions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.GetQuote
{
    public class GetQuoteQueryHandler : QueryHandler, IQueryHandler<GetQuoteQuery, GetQuoteVM>
    {
        public GetQuoteQueryHandler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<GetQuoteVM> Handle(GetQuoteQuery request, CancellationToken cancellationToken)
        {
            var entry = _context.Set<Quote>()
                            .Where(e => e.Id == request.QuoteId)
                                    .ProjectTo<GetQuoteDTO>(_mapper.ConfigurationProvider)
                                        .FirstOrDefault();

            if (entry == null)
            {
                throw new DomainException("Quote does not exists");
            }

            return await Task.Run(() =>
            {
                return new GetQuoteVM
                {
                    Entry = entry
                };
            });
        }
    }
}
