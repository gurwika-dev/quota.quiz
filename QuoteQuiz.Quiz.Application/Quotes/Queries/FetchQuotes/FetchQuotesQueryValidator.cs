﻿using FluentValidation;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesQueryValidator : AbstractValidator<FetchQuotesQuery>
    {
        public FetchQuotesQueryValidator()
        {
            RuleFor(x => x.Model).NotNull();
            RuleFor(x => x.Model.Page).GreaterThanOrEqualTo(1).NotNull();
            RuleFor(x => x.Model.Limit).GreaterThan(1).NotNull();
            RuleFor(x => x.Model.KeyWord).MaximumLength(50);
        }
    }
}
