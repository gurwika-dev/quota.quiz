﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using QuoteQuiz.Common.Application.Context.Abstraction;
using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using QuoteQuiz.Common.Application.Common.Handlers;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesQueryHandler : QueryHandler, IQueryHandler<FetchQuotesQuery, FetchQuotesVM>
    {
        public FetchQuotesQueryHandler(IApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<FetchQuotesVM> Handle(FetchQuotesQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<Quote>().Where(x => true);

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                entriesQuery = entriesQuery.Where(e => e.Text.Contains(request.Model.KeyWord));
            }

            var totalCount = entriesQuery.Count();
            var entries = await entriesQuery
                                    .ProjectTo<FetchQuotesDTO>(_mapper.ConfigurationProvider)
                                    .Skip(request.Model.Limit * (request.Model.Page - 1))
                                    .Take(request.Model.Limit)
                                    .ToListAsync(cancellationToken);

            foreach(var entry in entries)
            {
                entry.numberOfQuizzes = _context.Set<QuoteApplicationUser>()
                                        .Where(x => x.QuoteId == entry.Id)
                                        .Count();
            }

            return new FetchQuotesVM
            {
                Entries = entries,
                TotalCount = totalCount
            };
        }
    }
}
