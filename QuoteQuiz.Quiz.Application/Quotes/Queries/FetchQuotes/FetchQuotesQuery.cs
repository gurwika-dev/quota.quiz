﻿using QuoteQuiz.Common.Domain.Common.Abstraction.Queries;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesQuery : IQuery<FetchQuotesVM>
    {
        public FetchQuotesModel Model { get; set; }
    }
}
