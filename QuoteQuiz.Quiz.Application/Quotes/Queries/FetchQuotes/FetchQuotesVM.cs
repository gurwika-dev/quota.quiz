﻿using System.Collections.Generic;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesVM
    {
        public IList<FetchQuotesDTO> Entries { get; set; }
        public int TotalCount { get; set; }
    }
}
