﻿namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesModel
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string KeyWord { get; set; }
    }
}
