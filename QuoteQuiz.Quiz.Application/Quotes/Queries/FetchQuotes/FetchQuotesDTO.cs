﻿using AutoMapper;
using QuoteQuiz.Common.Application.Mappings.Abstraction;
using QuoteQuiz.Common.Domain.Entities.Quiz;
using System;

namespace QuoteQuiz.Quiz.Application.Quotes.Queries.FetchQuotes
{
    public class FetchQuotesDTO : IMapFrom<Quote>
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public Guid AuthorId { get; set; }
        public int numberOfQuizzes { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Quote, FetchQuotesDTO>();
        }
    }
}
